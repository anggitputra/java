package com.anggit.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        /**
         * manual array
         * array with arraylist
         */
//        basicArray();

        /**
         * manual insert array
         * note:
         * - array tidak bisa di hapus kecuali di update secara manual
         */
        insertUpdateArray();

    }

    public static void basicArray(){
        String[] city = {"Jakarta", "Bandung", "Solo", "Surabaya"};
        System.out.println("City =>"+ Arrays.toString(city));
        System.out.println("City =>"+ city[1]);

        String[][] student = {
                {"Bilal", "Mahendra"},
                {"Ahmad", "Malik"}
        };
        System.out.println("First Name: "+ student[0][0] + " Last name: "+ student[0][1]);

        ArrayList<String> listNumber = new ArrayList<>();
        for (int i = 0; i<2; i++) {
            listNumber.add("Number "+ i);
        }
        System.out.println("List Number: "+listNumber);
    }

    public static String[] data = new String[2];
    public static Scanner scn = new Scanner(System.in);
    public static void  insertUpdateArray(){
        data[0] = "Budi";
        data[1] = "Bagus";
        showData();
        while (true){
            System.out.println(" ");
            System.out.println("Option: ");
            System.out.println("1 (insert)");
            System.out.println("2 (exit)");

            System.out.println("Insert Choice: ");
            String choice = scanInput();

            if (choice.equals("1"))addArray();
            if  (choice.equals("2")) break;
        }
    }

    public static void showData(){
        System.out.println("List Name: ");
        for (int i= 0; i < data.length; i++){
            System.out.println(i+1 +". "+ data[i]);
        }
    }

    public static String scanInput(){
        return scn.nextLine();
    }

    public static void  addArray(){
        showData();
        System.out.println("Insert Name: ");
        String name = scanInput();

        String[] temp = data;
        Boolean isFull = false;
        for (String listName:data) {
            if (listName!= null) isFull = true;
        }
        if (isFull){
            data = new String[data.length+1];
            for (int i=0; i<temp.length; i++){
                data[i] = temp[i];
            }
        }
        for (int i=0; i<data.length; i++){
            if (data[i] == null){
                data[i]=name;
                break;
            }
        }
        showData();
    }
}
