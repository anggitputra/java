package com.anggit.exception;

/**
 * extend error
 */
public class ErrorException extends Error{
    public ErrorException(String message){
        super(message);
    }
}
