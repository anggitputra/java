package com.anggit.exception;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {
    public static void main(String[] args) {

//        exception();
//        stactTrace();
//        mainMultiStactTrace();
//
        /**
         * Try with resource
         * bertujuan untuk menutup koneksi sehingga tidak memakan resource
         */
        try {
            closeResource();
        }catch (Exception e){
            System.out.println("Error "+ e);
        }
    }

    public static void exception(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.username="Bintang";
        loginRequest.password="B12021";

        /**
         * Validation exception
         * wajib menggunakan try catch
         */
        try {
            ValidationUtils.validate(loginRequest);
        }catch (ValidationException | NullPointerException e){
            System.out.println("Error Message: "+ e.getMessage());
        }finally {
            System.out.println("Always execute");
        }

        /**
         * Runtime Exception
         * diwajibkan menggunakan try catch supaya program tidak berhenti
         */
        try {
            ValidationUtils.validateRuntime(loginRequest);
        }catch (BlankException | NullPointerException e){
            System.out.println("Error Message: "+ e.getMessage());
        }finally {
            System.out.println("Always execute");
        }

        /**
         * Error Exception
         * tidak perlu try catch sigunakan untuk error yang fatal dan menghentikan aplikasi
         */
        if (loginRequest.username == null || loginRequest.password == null){
            throw new ErrorException("Not allowed");
        }
    }

    public static void stactTrace(){
        try {
            String[] name ={"Budi","Edi"};
            System.out.println(name[2]);
        }catch (Throwable throwable){
            StackTraceElement[] stackTraceElements = throwable.getStackTrace();
            throwable.printStackTrace();
        }
    }

    public static void mainMultiStactTrace(){
        try {
            multiStackTrace();
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    public static void multiStackTrace(){
        try {
            String[] name ={"Budi","Edi"};
            System.out.println(name[2]);
        }catch (Throwable throwable){
            throw new RuntimeException(throwable);
        }
    }

    public static void closeResource() throws FileNotFoundException {
        //buffer reader untuk membaca file
        BufferedReader reader = new BufferedReader(new FileReader("name.md"));
        try(reader) {
            while (true) {
                //readline untuk mengambil per baris
                String line = reader.readLine();
                if (line == null) break;
                System.out.println(line);
            }
        }catch (Throwable throwable){
            System.out.println("Error read file "+ throwable.getMessage());
        }
    }

}
