package com.anggit.exception;

public class ValidationUtils {
    public static void validate(LoginRequest loginRequest) throws ValidationException, NullPointerException{
        if (loginRequest.username == null){
            throw new NullPointerException("Username is NUll");
        }else if (loginRequest.password.isBlank()){
            throw new ValidationException("Username is Blank");
        }
    }

    public static void validateRuntime(LoginRequest loginRequest){
        if (loginRequest.username == null){
            throw new NullPointerException("Username is NUll");
        }else if (loginRequest.password.isBlank()){
            throw new BlankException("Username is Blank");
        }
    }
}
