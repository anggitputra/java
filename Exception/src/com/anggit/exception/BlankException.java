package com.anggit.exception;

/**
 * runtime exception
 */
public class BlankException extends RuntimeException{
    public BlankException(String message){
        super(message);
    }
}
