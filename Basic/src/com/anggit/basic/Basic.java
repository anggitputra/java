package com.anggit.basic;   //package

public class Basic {    //class
    /*main first running in program
    {} = block code*/

    public static void main(String[] args) { //method

    /*
        writing style code
        cameCase = variable name, methode name and object name
        PascalCase = class name only
        ALL_UPPER = for generate constanta
    */
        final String ID_CARD_NAME = "Indra Herlambang";

        String name = "Indra Herlambang 012";

        Character Name  = 'a'; //character only
        if (Character.isAlphabetic(Name))
            System.out.println("yes");
        else
            System.out.println("no");

        System.out.println(ID_CARD_NAME);
        System.out.println(Name);
        System.out.println(name);

       /*
        note :
            Java is case sensitive
        */
    }
}
