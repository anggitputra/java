package com.anggit.service;

public interface Operation {
    Integer addition(Integer a, Integer b);
    //default tidak perlu implement di child
    default Integer subtraction(Integer a, Integer b){
        return a - b;
    }

}
