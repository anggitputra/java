package com.anggit.main;

import com.anggit.country.Country;
import com.anggit.service.Operation;
import com.anggit.service.OperationImpl;

public class Main extends Arithmetic{
    public static void main(String[] args) {
        /**
         * Country is another package
         * Indonesia is method in country
         */
        Country.indonesia();

        /**
         * Abstract bisa untuk menempatkan code yang sering di panggil
         * addition merupakan method from Arithmetic
         */
        String result = addition();
        System.out.println("Abstract =>"+result);

        /**
         * Getter and Setter
         * Getter bertujuan untuk mengambil value
         * Setter bertujuan mengisi value
         * berguna untuk validation
         */
        Employee employee = new Employee();
        employee.setName("Angga");
        employee.setPosition("Junior");
        employee.setStillWorking(true);
        System.out.println("Name =>"+employee.getName());
        System.out.println("Position  =>"+employee.getPosition());
        System.out.println("Still Working =>"+employee.getStillWorking());

        /**
         * Interface
         * method di dalam interface otomatis meruopan abstract
         */
        Operation operation = new OperationImpl();
        System.out.println("Res Addition =>"+operation.addition(1,5));
        System.out.println("Res Subtraction =>"+operation.subtraction(5,1));
    }

}

