package com.anggit.main;

public class Employee {
    String name;
    String position;
    Boolean stillWorking;

    public void setName(String name) {
        this.name = name;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setStillWorking(Boolean stillWorking) {
        this.stillWorking = stillWorking;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public Boolean getStillWorking() {
        return stillWorking;
    }
}
