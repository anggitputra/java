package com.anggit.putra.validation;

import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.util.Set;

public class ConstructorValidationTest extends AbstractValidatorTest{

    @Test
    void testValidationConstructorParameter() throws NoSuchMethodException{
        String firstName = "";
        String lastName = "";
//        Address address = null;
        Address address = new Address();

        Constructor<Person> constructor = Person.class.getConstructor(String.class, String.class, Address.class);
        Set<ConstraintViolation<Object>> violations = executableValidator.
                validateConstructorParameters(constructor, new Object[]{firstName, lastName, address});
        for (ConstraintViolation<Object> violation : violations){
            System.out.println(violation.getPropertyPath());
            System.out.println(violation.getMessage());
            System.out.println("================");
        }

        /*
        result with address null
        Person.firstName
        first name can not blank
        ================
        Person.lastName
        last name can not blank
        ================
        Person.address
        address can not null
        ================
         */

        /*
        result with address
        Person.lastName
        last name can not blank
        ================
        Person.address.street
        Street can not blank
        ================
        Person.firstName
        first name can not blank
        ================
        Person.address.country
        Country can not blank
        ================
        Person.address.city
        City can not blank
        ================
         */
    }

    @Test
    void testValidationConstructorReturnValue() throws NoSuchMethodException {
        //problem
        String firstName = "";
        String lastName = "";
        Address address = new Address();
        Person person = new Person(firstName, lastName, address);

        Constructor<Person> constructor = Person.class.getConstructor(String.class, String.class, Address.class);

        Set<ConstraintViolation<Object>> violations = executableValidator.
                validateConstructorReturnValue(constructor, person);
        for (ConstraintViolation<Object> violation : violations) {
            System.out.println(violation.getPropertyPath());
            System.out.println(violation.getMessage());
            System.out.println("================");
        }
    }
}
