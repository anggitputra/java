package com.anggit.putra.validation;

import com.anggit.putra.validation.group.VirtualAccountPaymentGroup;
import org.junit.jupiter.api.Test;

public class MessageInterpolationTest extends AbstractValidatorTest{
    @Test
    void testMessage() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("10238138193810312938");
        payment.setVirtualAccount("312313");
        payment.setAmount(10L);

        validateWithGroup(payment, VirtualAccountPaymentGroup.class);

        /*
        result
        customer
        customer can not null
        ======================
        amount
        amount must between 10000 and 100000000
        ======================
        orderId
        order id must between 1 and 10 characters
        ======================
         */
    }
}
