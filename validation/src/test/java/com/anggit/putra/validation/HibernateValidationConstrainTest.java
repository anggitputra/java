package com.anggit.putra.validation;

import org.junit.jupiter.api.Test;

//abstract class fro validation data
public class HibernateValidationConstrainTest extends AbstractValidatorTest{
    @Test
    void hibernateValidationConstrainTest() {
        Payment payment = new Payment();
        payment.setAmount(120000L);
        payment.setOrderId("000021");
        payment.setCreditCard("443");

        validate(payment);
    }
}
