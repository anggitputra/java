package com.anggit.putra.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.*;

import java.util.Set;

public class ConstrainViolationTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    @BeforeEach
    void setUp(){
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    void testValidationFailed() {
        Person person = new Person();
        person.setFirstName("anggita");
        person.setLastName("dwi setiya putra");
        Set<ConstraintViolation<Person>> violations = validator.validate(person);
//        Assertions.assertEquals(2,violations.size());

        //akan menampilkan kesalahan
        for (ConstraintViolation<Person> violation : violations){
            System.out.println(violation);
            System.out.println("Message :"+ violation.getMessage());
            System.out.println("Bean :"+ violation.getLeafBean());
            System.out.println("Constraint :"+ violation.getConstraintDescriptor().getAnnotation());
            System.out.println("Invalid Value :"+ violation.getInvalidValue());
            System.out.println("Path :"+ violation.getPropertyPath());
        }
    }

    @AfterEach
    void tearDown() {
        validatorFactory.close();
    }
}
