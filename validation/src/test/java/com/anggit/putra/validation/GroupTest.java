package com.anggit.putra.validation;

import com.anggit.putra.validation.group.CreditCardPaymentGroup;
import com.anggit.putra.validation.group.VirtualAccountPaymentGroup;
import org.junit.jupiter.api.Test;

public class GroupTest  extends AbstractValidatorTest{

    @Test
    void creditCardGroupTest() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("0001");
        payment.setAmount(20_000L);
        payment.setCreditCard("12345");

        validateWithGroup(payment, CreditCardPaymentGroup.class);
    }

    @Test
    void virtualAccountTest() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("0011");
        payment.setAmount(20_000L);
        payment.setVirtualAccount("");

        validateWithGroup(payment, VirtualAccountPaymentGroup.class);
    }
}
