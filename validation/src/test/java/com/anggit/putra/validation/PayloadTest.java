package com.anggit.putra.validation;

import com.anggit.putra.validation.group.CreditCardPaymentGroup;
import com.anggit.putra.validation.payload.EmailErrorPayload;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Payload;
import org.junit.jupiter.api.Test;

import java.util.Set;

public class PayloadTest extends AbstractValidatorTest{
    @Test
    void payloadTest() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("0002");
        payment.setAmount(33_000L);
        payment.setCreditCard("311111");

        Set<ConstraintViolation<Object>> violations = validator.validate(payment, CreditCardPaymentGroup.class);
        for (ConstraintViolation<Object> violation : violations){
            System.out.println(violation.getPropertyPath());
            System.out.println(violation.getMessage());
            Set<Class<? extends Payload>> payloads = violation.getConstraintDescriptor().getPayload();
            for (Class<? extends Payload> payload : payloads){
                EmailErrorPayload emailErrorPayload = new EmailErrorPayload();
                emailErrorPayload.sendEmail(violation);
            }
            System.out.println("======================");
        }
    }
}
