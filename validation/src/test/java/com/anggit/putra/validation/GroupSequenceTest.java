package com.anggit.putra.validation;

import com.anggit.putra.validation.group.PaymentV2Group;
import org.junit.jupiter.api.Test;

public class GroupSequenceTest extends AbstractValidatorTest{
    @Test
    void groupSequenceTest() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("00022");
        payment.setAmount(12_000L);
        payment.setCreditCard("4111111111111111");
        payment.setVirtualAccount("dasdasdsa7dsaudhasdh");
        validateWithGroup(payment, PaymentV2Group.class);
    }
}
