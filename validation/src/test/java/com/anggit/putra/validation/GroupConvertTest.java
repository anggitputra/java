package com.anggit.putra.validation;

import com.anggit.putra.validation.group.CreditCardPaymentGroup;
import org.junit.jupiter.api.Test;

public class GroupConvertTest extends  AbstractValidatorTest{
    @Test
    void groupConvertTest() {
        Paymentv2 payment = new Paymentv2();
        payment.setOrderId("0112");
        payment.setAmount(14_000L);
        payment.setCreditCard("4111111111111111");
        payment.setCustomer(new Customer());

        validateWithGroup(payment, CreditCardPaymentGroup.class);
    }
}
