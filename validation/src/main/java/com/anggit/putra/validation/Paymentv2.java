package com.anggit.putra.validation;

import com.anggit.putra.validation.group.CreditCardPaymentGroup;
import com.anggit.putra.validation.group.VirtualAccountPaymentGroup;
import com.anggit.putra.validation.payload.EmailErrorPayload;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.ConvertGroup;
import jakarta.validation.groups.Default;
import org.hibernate.validator.constraints.LuhnCheck;
import org.hibernate.validator.constraints.Range;

public class Paymentv2 {
    @NotBlank(groups = {CreditCardPaymentGroup.class, VirtualAccountPaymentGroup.class},
            message ="order id can not blank")
//            message = "{order.id.notblank}")
    @Size(groups = {CreditCardPaymentGroup.class, VirtualAccountPaymentGroup.class}, min = 1, max = 10,
            message = "order id must between {min} and {max} characters")
//            message = "{order.id.size}")
    private String orderId;

    @NotNull(groups = {CreditCardPaymentGroup.class, VirtualAccountPaymentGroup.class},message = "amount can not null")
    @Range(groups = {CreditCardPaymentGroup.class, VirtualAccountPaymentGroup.class},min = 10_000L, max = 100_000_000,
            message = "amount must between {min} and {max}")
//            message = "{order.amount.range}")
    private Long amount;

    @LuhnCheck(groups = {CreditCardPaymentGroup.class},message = "invalid credit card number", payload = {EmailErrorPayload.class})
    @NotBlank(groups = {CreditCardPaymentGroup.class},message = "credit card can not blank")
    private String creditCard;

    @NotBlank(groups = {VirtualAccountPaymentGroup.class},message = "virtual account can not blank")
    private String virtualAccount;

    @Valid
    @NotNull(groups = {VirtualAccountPaymentGroup.class,CreditCardPaymentGroup.class},
            message = "customer can not null")
    @ConvertGroup(from = VirtualAccountPaymentGroup.class, to = Default.class)
    @ConvertGroup(from = CreditCardPaymentGroup.class, to = Default.class)
    private  Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(String virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    @Override
    public String toString() {
        return "Paymentv2{" +
                "orderId='" + orderId + '\'' +
                ", amount=" + amount +
                ", creditCard='" + creditCard + '\'' +
                ", virtualAccount='" + virtualAccount + '\'' +
                ", customer=" + customer +
                '}';
    }

    //    @Override
//    public String toString() {
//        return "Payment{" +
//                "orderId='" + orderId + '\'' +
//                ", amount=" + amount +
//                ", creditCard='" + creditCard + '\'' +
//                ", virtualAccount='" + virtualAccount + '\'' +
//                '}';
//    }
}
