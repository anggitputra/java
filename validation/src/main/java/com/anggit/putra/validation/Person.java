package com.anggit.putra.validation;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public class Person {
    @NotBlank(message = "first name not blank")
    @Size(max = 20, message = "max 20 character")
    private String firstName;

    @NotBlank(message = "last name not blank")
    @Size(max = 20, message = "max 20 character")
    private String lastName;

    @NotNull(message = "Address can not null")
    @Valid //untuk validation object
    private Address address;

    @Valid
    public Person() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Person(@NotBlank(message = "first name can not blank") String firstName,
                  @NotBlank(message = "last name can not blank")String lastName,
                  @NotNull(message = "address can not null") @Valid Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    //    public Person(String firstName, String lastName) {
//        this.firstName = firstName;
//        this.lastName = lastName;
//    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    //validation method di parameter
    public void sayHello(@NotBlank(message = "Name can not blank") String name){
        System.out.println("Hello" + name + ", my name is "+ firstName);
    }

    //validation return value
    @NotBlank(message = "Full name can not blank")
    public String fullName(){
        return firstName + " " + lastName;
    }
}
