package com.anggit.putra.javareflection.validation;

import com.anggit.putra.javareflection.annotation.NotBlank;

import java.lang.reflect.Field;

public class Validator {
    public static void validationNotBlank(Object object) throws IllegalAccessException {

        //get class
        Class<?> aClass = object.getClass();
        //get all field from class
        Field[] fields = aClass.getDeclaredFields();
        //looping check field
        for (Field field : fields){
            //get annotation not blank
            NotBlank notBlank = field.getAnnotation(NotBlank.class);
            //check
            if (notBlank != null){
                //get field value
                field.setAccessible(true);
                String value = (String) field.get(object);
                if (notBlank.allowEmptyString()){
                    //ignore
                }else{
                    //delete white space
                    value = value.trim();
                }

                if ("".equals(value)){
                    //return error
                    throw new RuntimeException(field.getName()  +" must be not blank");
                }
            }
        }
    }
}
