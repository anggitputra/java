package com.anggit.putra.javareflection.data;

public interface Nameable {
    String getFirstName();
    String getLastName();
}
