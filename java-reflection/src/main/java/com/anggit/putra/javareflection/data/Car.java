package com.anggit.putra.javareflection.data;

public interface Car {
    void run();

    String getName();
}
