package com.anggit.putra.javareflection;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;

public class ModifierTest {
    @Test
    void modifierTest() {
        Class<Person> personClass = Person.class;

        System.out.println(Modifier.isPublic(personClass.getModifiers()));
        System.out.println(Modifier.isPrivate(personClass.getModifiers()));
        System.out.println(Modifier.isStatic(personClass.getModifiers()));
        System.out.println(Modifier.isFinal(personClass.getModifiers()));

    }
}
