package com.anggit.putra.javareflection;

import org.junit.jupiter.api.Test;

public class SupperClassTest {
    @Test
    void getSupperClassTest() {
        Class<Person> personClass = Person.class;
        System.out.println(personClass);

        Class<? super Person> objectClass = personClass.getSuperclass();
        System.out.println(objectClass);

        Class<? super Person> nullClass = objectClass.getSuperclass();
        System.out.println(nullClass);
    }
}
