package com.anggit.putra.javareflection;

import com.anggit.putra.javareflection.validation.Validator;
import org.junit.jupiter.api.Test;

public class AnnotationTest {

    @Test
    void validationUsingAnnotationTest() throws IllegalAccessException {
//        Person person = new Person("Anggit", "Putra");
        Person person = new Person("Anggit", " ");
        Validator.validationNotBlank(person);
    }
}
