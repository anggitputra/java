package com.anggit.putra.javareflection;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class PackageTest {
    @Test
    void packageTest() {
        Class<Person> personClass = Person.class;

        Package apackage = personClass.getPackage();
        System.out.println(apackage.getName());
        System.out.println(Arrays.toString(apackage.getAnnotations()));
    }
}
