package com.anggit.putra.javareflection;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

public class FieldTest {
    @Test
    void getFieldTest() {
        Class<Person> person = Person.class;

        Field[] fields= person.getDeclaredFields();
        for (Field field : fields){
            //get list field in person
            System.out.println(field.getName() + " : " + field.getType().getName());;
        }
    }

    @Test
    void getFieldValueTest() throws NoSuchFieldException, IllegalAccessException {
        Class<Person> personClass = Person.class;
        Field firstName = personClass.getDeclaredField("firstName");
        firstName.setAccessible(true);

        Person person1 = new Person("Anggit", "Putra");//set value to field firstname
        Person person2 = new Person("Frans", "Sisco");
        firstName.set(person2,"Sanoval"); //update value firstname

        String result = (String) firstName.get(person1);
        String result2 = (String) firstName.get(person2);
        System.out.println("Persone 1 name :"+result +" , Person 2 Name :"+ result2);
    }
}
