package com.anggit.putra.javareflection;

import com.anggit.putra.javareflection.data.Nameable;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class InterfaceTest {
    @Test
    void ClassTest() {
        Class<Nameable> nameAbleClass= Nameable.class;

        System.out.println(nameAbleClass.getName());
        System.out.println(nameAbleClass.isInterface());
        System.out.println(nameAbleClass.getSuperclass());
        System.out.println(Arrays.toString(nameAbleClass.getInterfaces()));
        System.out.println(Arrays.toString(nameAbleClass.getDeclaredFields()));
        System.out.println(Arrays.toString(nameAbleClass.getDeclaredMethods()));
        System.out.println(Arrays.toString(nameAbleClass.getDeclaredConstructors()));
    }

    @Test
    void supperInterfacesTest() {
        Class<Person> personClass = Person.class;

        System.out.println(Arrays.toString(personClass.getInterfaces()));
    }
}
