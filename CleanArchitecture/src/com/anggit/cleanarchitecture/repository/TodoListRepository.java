package com.anggit.cleanarchitecture.repository;

import com.anggit.cleanarchitecture.entity.TodoList;

public interface TodoListRepository {
    TodoList[] getAll();

    void add(TodoList todoList);

    boolean remove(Integer number);

}
