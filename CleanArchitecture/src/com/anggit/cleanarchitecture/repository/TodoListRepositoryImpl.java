package com.anggit.cleanarchitecture.repository;

import com.anggit.cleanarchitecture.entity.TodoList;

/**
 * service impl sebagai pendukung todolistrespository
 * didalam impl ini dibuat method yang berkaitan dengan data atau entity
 */
public class TodoListRepositoryImpl implements TodoListRepository {

    public TodoList[] data = new TodoList[10];

    @Override
    public TodoList[] getAll() {
        return data;
    }

    //checking data kosong atau tidak
    public boolean isFull(){
        var isFull = true;
        for (int i = 0; i < data.length; i++){
            if (data[i] == null){
                isFull = false;
                break;
            }
        }
        return isFull;
    }

    public void resizeIfFull(){
        //check todolist full atau tidak dan  jika penuh maka di resize
        if (isFull()){
            var temp = data;
            data = new TodoList[data.length * 2];

            for (int i=0; i<temp.length; i++){
                data[i]=temp[i];
            }
        }
    }

    @Override
    public void add(TodoList todoList) {
        resizeIfFull();
        //menambahkan value baru ke todolist yang valunya null
        for (var i=0; i<data.length; i++){
            if (data[i] == null){
                data[i] = todoList;
                break;
            }
        }
    }

    @Override
    public boolean remove(Integer number) {
        if ((number-1) >= data.length){
            return false;
        }else if (data[number - 1 ] == null){
            return false;
        }else {
            //for ini akan mencari nilai yang sama sampai ketemu dan akan di set null
            for (int i=(number - 1); i<data.length; i++){
                if (i == (data.length - 1)){
                    data[i] = null;
                }else {
                    data[i] = data[i + 1];
                }
            }
            return true;
        }
    }
}
