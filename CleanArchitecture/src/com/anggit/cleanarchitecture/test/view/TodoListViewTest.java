package com.anggit.cleanarchitecture.test.view;

import com.anggit.cleanarchitecture.repository.TodoListRepository;
import com.anggit.cleanarchitecture.repository.TodoListRepositoryImpl;
import com.anggit.cleanarchitecture.service.TodoListService;
import com.anggit.cleanarchitecture.service.TodoListServiceImpl;
import com.anggit.cleanarchitecture.view.TodoListView;

public class TodoListViewTest {
    public static void main(String[] args) {
//        testShowTodoList();
//        testAddTodoList();
        testRemoveTodoList();
    }
    public static void testShowTodoList(){
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);
        TodoListView todoListView = new TodoListView(todoListService);

        todoListService.addTodoList("Jakarta");
        todoListService.addTodoList("Semarang");
        todoListService.addTodoList("Jogjakarta");

        todoListView.showTodoList();

    }

    public static void testAddTodoList(){
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);
        TodoListView todoListView = new TodoListView(todoListService);

        todoListView.showTodoList();
        todoListService.showTodoList();

    }

    public static void testRemoveTodoList(){
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);
        TodoListView todoListView = new TodoListView(todoListService);

        todoListService.addTodoList("Jakarta");
        todoListService.addTodoList("Semarang");
        todoListService.addTodoList("Jogjakarta");

        todoListService.showTodoList();

        todoListView.removeTodoList();

        todoListService.showTodoList();
    }
}
