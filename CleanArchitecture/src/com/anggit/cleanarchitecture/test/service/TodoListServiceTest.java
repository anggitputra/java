package com.anggit.cleanarchitecture.test.service;

import com.anggit.cleanarchitecture.entity.TodoList;
import com.anggit.cleanarchitecture.repository.TodoListRepository;
import com.anggit.cleanarchitecture.repository.TodoListRepositoryImpl;
import com.anggit.cleanarchitecture.service.TodoListService;
import com.anggit.cleanarchitecture.service.TodoListServiceImpl;

public class TodoListServiceTest {
    public static void main(String[] args) {
//        testShowTodoList();
//        testAddTodoList();
        testRemoveTodoList();
    }
    public static void testShowTodoList(){
        TodoListRepositoryImpl todoListRepository = new TodoListRepositoryImpl();
        todoListRepository.data[0] = new TodoList("Jakarta");
        todoListRepository.data[1] = new TodoList("Bandung");
        todoListRepository.data[2] = new TodoList("Jogjakarta");

        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);

        todoListService.showTodoList();
    }

    public static void testAddTodoList(){
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);

        todoListService.addTodoList("Semarang");
        todoListService.addTodoList("Banten");
        todoListService.addTodoList("Surabaya");

        todoListService.showTodoList();
    }

    public static void testRemoveTodoList(){
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);

        todoListService.addTodoList("Semarang");
        todoListService.addTodoList("Banten");
        todoListService.addTodoList("Surabaya");

        todoListService.showTodoList();

        todoListService.removeTodoList(4);
        todoListService.removeTodoList(2);

        todoListService.showTodoList();
    }
}
