package com.anggit.cleanarchitecture;

import com.anggit.cleanarchitecture.repository.TodoListRepository;
import com.anggit.cleanarchitecture.repository.TodoListRepositoryImpl;
import com.anggit.cleanarchitecture.service.TodoListService;
import com.anggit.cleanarchitecture.service.TodoListServiceImpl;
import com.anggit.cleanarchitecture.view.TodoListView;

public class ApplicationTodoList {
    public static void main(String[] args) {
        TodoListRepository todoListRepository = new TodoListRepositoryImpl();
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);
        TodoListView todoListView = new TodoListView(todoListService);

        todoListView.showTodoList();
    }
}
