Clean Architecture

-dibagi menjadi 4 layer
    -Enterprise Bussines Rulles = Entities 
    -Application Byssines Rules = Usecase
    -Interface adapter = controller, gateway, presenters
    -Framework and driver = Device, Db, External intervace, Springboot, Web

-Versi Java
    Web Destop -> Service
    Service -> Repository
    Repository -> Entity
    Repositoy -> Database

-> pengelompokan
    - Repository(untuk pemrosesan data yang berkaitan dengan entity)
    - Entity(yang merupakan struct atau bisa disebut entity table)
    - service ( yang berhubungan denang repository dan tidak disarankan ada koneksi ke db)
    - view(yang di tampilkan ke user dan terhubung ke service)
    - util(penunjanga dari program yang di inginkan dan dibuat global)