package com.anggit.conditionandloop;

public class ConditionAndLoop {
    public static void main(String[] args) {
        //Condition
//            ifFunction(); //best for 2 condition
            switchFunction(); //best for multiple condition

        //Looping
//        forFunction(); //often use for
//        foreachFunction(); //often use for
//        whileFunction();
//        doWhileFunction(); //be careful use this function
    }

    public static void ifFunction(){
        int a = 8;
        if (a > 5)
            System.out.println("Pass");
        else
            System.out.println("Failed");


        //else if node recommended
        if (a > 9)
            System.out.println("Very Good");
        else if(a > 6)
            System.out.println("Good");
        else
            System.out.println("Bad");

    }

    public static void switchFunction(){

        //manual
        int day = 3;
        switch (day){
            case  1:
                System.out.println("Monday");
                break;
            case  2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            default:
                System.out.println("Free Days");
        }

        //lamda support in java > 14
//            case "A" -> System.out.println("Monday");
//            default -> System.out.println("Free Days");
//        }

        //yield support in java > 14
//        String result = switch (day){
//            case 3:
//                yield "Wednesday";
//            default:
//                yield "Free Days";
//        };
//        System.out.println("result =>"+result);


    }

    public static void forFunction(){
        for (int i=0; i<5; i++){
            System.out.println("For "+i);
        }
    }

    public static void foreachFunction(){
        int[] bill = new int[]{1,2,3,4,5};
        for (int data: bill) {
            System.out.println("Foreach "+data);
        }
    }

    public static void whileFunction(){
        int a = 5;
        while (a<10) {
            System.out.println("While " + a);
            a++;
        }
    }

    public static void doWhileFunction(){
        int a = 1;
        do {
            System.out.println("DoWhile " + a);
            a++;
        } while (a < 5);
    }
}
