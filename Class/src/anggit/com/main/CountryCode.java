package anggit.com.main;

public enum CountryCode {
    ID("Indonesia"),
    IN("India"),
    IT("Italy");

    private String description;
    CountryCode(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
