package anggit.com.main;

public class Main {

    public static final String CODEJAWA = "JW13";

    public static void main(String[] args) {
        /**
         * class country setting final
         * final bisa berada di class method dan variabel tunjuannya dibuat menjadi tetap
         */
        Country country = new Country();
        country.indonesia();

        /**
         * Province sebagai kelas utama
         * District sebagai ineer class(berada dalam class Province)
         * inner class bisa membaca private key antar class
         * Country sebagai outer class
         */
        Province province = new Province();
        province.setName("Jawa Tengah");

        Province.District district = province.new District();
        district.setName("Solo");

        System.out.println(district.getName());
        System.out.println(district.getCompany());

        /**
         * Static
         * CODEJAWA sebagai static Variable
         * Static bisa digunakan class, method, import dll
         */
        System.out.println(CODEJAWA);

        /**
         * Enum
         * Enum class sangat berguna untuk membuat nilai yang di set seperti level, status
         */
        String indonesia = CountryCode.ID.name();
        String description = CountryCode.ID.getDescription();
        System.out.println("Code ID: "+ indonesia);
        System.out.println("Code ID Description: "+ description);
        CountryCode id = CountryCode.ID;
        System.out.println("Indonesia Code: "+id);

    }
}
