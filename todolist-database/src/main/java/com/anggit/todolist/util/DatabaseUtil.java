package com.anggit.todolist.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DatabaseUtil {
    private static HikariDataSource dataSource;

    static {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");//driver yang dipakai
        config.setJdbcUrl("jdbc:mysql://localhost:3306/java_todolist?serverTimezone=Asia/Jakarta");//table dan url serta set time database
        config.setUsername("root");
        config.setPassword("root");

        config.setMaximumPoolSize(10);
        config.setMinimumIdle(5);
        config.setIdleTimeout(60_000);
        config.setMaxLifetime(10 * 60_000);

        dataSource = new HikariDataSource(config); //untuk export semua config ke datasource
    }

    public static HikariDataSource getDataSource() {
        return dataSource;
    }
}
