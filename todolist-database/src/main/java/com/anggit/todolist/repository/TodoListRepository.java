package com.anggit.todolist.repository;

import com.anggit.todolist.entity.TodoList;

public interface TodoListRepository {
    TodoList[] getAll();

    void add(TodoList todoList);

    boolean remove(Integer number);

}
