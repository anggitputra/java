package com.anggit.todolist.repository;


import com.anggit.todolist.entity.TodoList;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * service impl sebagai pendukung todolistrespository
 * didalam impl ini dibuat method yang berkaitan dengan data atau entity
 */
public class TodoListRepositoryImpl implements TodoListRepository {

    public TodoList[] data = new TodoList[10];

    private HikariDataSource dataSource; //inport datasource

    public TodoListRepositoryImpl(HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public TodoList[] getAll(){
        String sql = "select * from todolist";
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)){

            List<TodoList> list = new ArrayList<>();
            while (resultSet.next()){
                TodoList todoList = new TodoList();
                todoList.setId(resultSet.getInt("id"));
                todoList.setTodo(resultSet.getString("todo"));
                list.add(todoList);
            }
            return list.toArray(new TodoList[]{});
        } catch (SQLException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void add(TodoList todoList) {
        String sql = "insert into todolist(todo) values (?)";

        try (Connection connection = dataSource.getConnection()){
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setString(1, todoList.getTodo());
                statement.executeUpdate();
            }
        }catch (SQLException exception){
            throw new RuntimeException(exception);
        }
    }

    private boolean isExisting(Integer number){
        String sql = "select id from todolist where id = ?";
        try (Connection connection = dataSource.getConnection()){
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, number);
                try(ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) return true;
                    else return false;
                }
            }
        }catch (SQLException exception){
            throw new RuntimeException(exception);
        }
    }

    @Override
    public boolean remove(Integer number) {
        if (isExisting(number)) {
            String sql = "delete from todolist where id = ?";
            try (Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql)) {
                    statement.setInt(1, number);
                    statement.executeUpdate();
                    return true;
            } catch (SQLException exception) {
                throw new RuntimeException(exception);
            }
        }else return false;
    }
}
