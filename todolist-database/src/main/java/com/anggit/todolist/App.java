package com.anggit.todolist;

import com.anggit.todolist.repository.TodoListRepository;
import com.anggit.todolist.repository.TodoListRepositoryImpl;
import com.anggit.todolist.service.TodoListService;
import com.anggit.todolist.service.TodoListServiceImpl;
import com.anggit.todolist.util.DatabaseUtil;
import com.anggit.todolist.view.TodoListView;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 * Sebagai Main aplikasi untuk menjalankan semua
 *
 */
public class App 
{
    public static void main(String[] args) {
        HikariDataSource dataSource = DatabaseUtil.getDataSource(); // Initialisasi Connecting ke Database
        TodoListRepository todoListRepository = new TodoListRepositoryImpl(dataSource);
        TodoListService todoListService = new TodoListServiceImpl(todoListRepository);
        TodoListView todoListView = new TodoListView(todoListService);

        todoListView.showTodoList();
    }

}
