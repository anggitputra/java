package com.anggit.todolist.service;

import com.anggit.todolist.entity.TodoList;
import com.anggit.todolist.repository.TodoListRepository;

/**
 * sebagai implement dari todolist service
 * sebagai logic function yang akan berhubungan dengan repositoy
 */
public class TodoListServiceImpl implements TodoListService {

    private TodoListRepository todoListRepository;

    public TodoListServiceImpl(TodoListRepository todoListRepository) {
        this.todoListRepository = todoListRepository;
    }

    @Override
    public void showTodoList() {
        TodoList[] model = todoListRepository.getAll();
        System.out.println("Show Todolist");
        for (var todoList : model){
            System.out.println(todoList.getId() + "."+ todoList.getTodo());
        }
    }

    @Override
    public void addTodoList(String todo) {
        TodoList todoList = new TodoList(todo);
        todoListRepository.add(todoList);
        System.out.println("Success insert todo= "+ todo);
    }

    @Override
    public void removeTodoList(Integer number) {
        boolean success = todoListRepository.remove(number);
        if (success){
            System.out.println("Success delete todo= "+ number);
        }else {
            System.out.println("Failed delete todo= "+ number);
        }
    }
}
