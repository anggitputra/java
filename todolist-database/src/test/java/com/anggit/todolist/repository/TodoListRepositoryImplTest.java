package com.anggit.todolist.repository;

import com.anggit.todolist.entity.TodoList;
import com.anggit.todolist.util.DatabaseUtil;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TodoListRepositoryImplTest {

    private HikariDataSource dataSource;

    private TodoListRepository todoListRepository;

    @BeforeEach
    public void setUp(){
        dataSource = DatabaseUtil.getDataSource();
        todoListRepository = new TodoListRepositoryImpl(dataSource);
    }

    @Test
    public void addTest(){
        System.out.println("Insert Anang");
        TodoList todoList = new TodoList();
        todoList.setTodo("Anang");

        todoListRepository.add(todoList);
    }

    @Test
    public void removeTest(){
        System.out.println("Remove Id 1  = "+todoListRepository.remove(1));
        System.out.println("Remove Id 3  = "+todoListRepository.remove(3));
    }

    @Test
    public void getAllTest(){
        todoListRepository.add(new TodoList("Budi"));
        todoListRepository.add(new TodoList("Bela"));

        TodoList[] todoLists = todoListRepository.getAll();
        for (var todo : todoLists){
            System.out.println(todo.getId() + " : " + todo.getTodo());
        }
    }

    @AfterEach
    public void tearDown(){
        dataSource.close();
    }
}
