package com.anggit.datetime;

import org.junit.jupiter.api.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.*;
import java.util.*;

public class DateTest {
    @Test
    void dateTest() {
        var date = new Date(); //hanya ini yang bisa saat ini karena yang lain udah depreceted
        var date1 = new Date(System.currentTimeMillis());
        var date2 = new Date(339958800000L); //Fri Oct 10 00:00:00 WIB 1980

        System.out.println(date);
        System.out.println(date1);
        System.out.println(date2);
    }

    /**
     * Callendar untuk memanipulasi data callendar
     */
    @Test
    void calendar() {
        Calendar calendar = Calendar.getInstance(); //untuk memanipulasi calendar
//        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta")); // set callendar ke dengan timezone asia jakarta
//        calendar.setTimeZone(TimeZone.getTimeZone("GMT")); //timezone dirubah ke gmt +0
        calendar.set(Calendar.YEAR,2020); //manipulasi callendar sesuai yang di inginkan dan masih banyak lagi yang tersimpan di Calendar
        Date date  = calendar.getTime();
        System.out.println(date);
        System.out.println(date.getTime());//in number
        System.out.println(calendar.get(Calendar.YEAR)); //untuk mendapatkan tahun dan lain sebagainya
    }

    /**
     * Timezone merupakan versi lama
     */
    @Test
    void timezoneTest() {
        TimeZone timeZoneDef = TimeZone.getDefault(); //timezone yang digunakan system operasi
        System.out.println("Default timezone ="+timeZoneDef);
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        System.out.println("Time Zone Time :"+timeZone);
        String[] availableIDTimeZone = TimeZone.getAvailableIDs();
        System.out.println("Time Zone :"+ Arrays.asList(availableIDTimeZone));
    }

    /**
     * Local date yang didukung saat ini di java
     * dan samngat compatible ketimbang Date
     * pati perlu di ingat LocalDate bersifat imutable atau data yang tidak bisa dirubah
     * sehingga cocok untuk proses paralel
     * Local Date tidak ada waktu
     */
    @Test
    void localDateTest() {
        LocalDate localDate = LocalDate.now();
        LocalDate localDate1 = LocalDate.of(2020, Month.AUGUST, 17); //set localtime sesuai yang di inginkan
        LocalDate localDate2 = LocalDate.parse("2020-09-17"); //parsing string to date

        //manipulation localdate
        LocalDate localDateYear = localDate.withYear(2020).withMonth(10); //maipulation local date set year and mont dll
        LocalDate localDateYearPlus = localDate.plusYears(1); //pluss untuk menambah dan masih banyak lagi

        //getData
        Integer getYear = localDate.getYear(); //get year aja


        System.out.println(getYear);
        System.out.println(localDateYearPlus);
        System.out.println(localDateYear);
        System.out.println(localDate2);
        System.out.println(localDate1);
        System.out.println(localDate);
    }

    /**
     * localtime sama seperti localdate
     * tapi ini untuk bagian timenya
     * jam, menit, detik, milidetik
     */
    @Test
    void localTimeTest() {
        LocalTime localTime = LocalTime.now(); //set time sekarang
        LocalTime localTime1 = LocalTime.of(12,12); //set time yang di inginkan
        LocalTime localTime2 = LocalTime.parse("12:12"); //parsing string to time

        //untuk manipulation juga sama seperti LocalDate

        System.out.println(localTime2);
        System.out.println(localTime1);
        System.out.println(localTime);
    }

    /**
     * Localdatetime adalah versi lengkap karena pengabungkan dari local date dan local time
     * default formant tahun,bulan,hari,T,jam,menit,detik,nano second
     * untuk operasinya hanya digabungkan dan semua sama
     */
    @Test
    void localDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTime1 = LocalDateTime.of(2020, Month.SEPTEMBER, 20,20,20,30,560);

        //conversion
        //localDate to localDateTime
        LocalDate localDate = localDateTime.toLocalDate();
        System.out.println("datetime to date :"+ localDate);
        LocalDateTime localDateTime2 = localDate.atTime(10,10,10);//perlu di tambah atTime kalo untuk yang time ditambah atDate
        System.out.println("date to datetime :"+localDateTime2);
        //localTime to localDateTime
        LocalTime localTime = localDateTime.toLocalTime();
        LocalDateTime localTime1 = localTime.atDate(LocalDate.now());
        System.out.println("time to dateTime :"+ localTime1);

        System.out.println(localDateTime1);
        System.out.println(localDateTime);
    }

    /**
     * konsep year month and day sama seperti localdatetime dan lain lain
     * baik konversi dan lainya
     */
    @Test
    void yearMontDayTest() {
        Year year = Year.now();
        Year year1 = Year.of(2020); //set year
        Year year2 = Year.parse("2020"); //set year with string
        YearMonth yearMonth= YearMonth.now();//year mont = 2021-08
        MonthDay monthDay = MonthDay.now();//--08-17
        MonthDay monthDay1 = MonthDay.parse("--10-08"); //didpean dikasih -- karena tidak akan menampilkan tahun

        //conversion
        LocalDate localDate = year.atMonth(Month.AUGUST).atDay(27);//2021-08-27
        LocalDateTime localDateTime = year.atMonth(Month.MAY).atDay(12).atTime(LocalTime.now()); //2021-05-12T15:55:54.942782 

        System.out.println(localDateTime);
        System.out.println(localDate);
        System.out.println(monthDay1);
        System.out.println(monthDay);
        System.out.println(yearMonth);
        System.out.println(year2);
        System.out.println(year1);
        System.out.println(year.getValue()); //mengambil value dai year
        System.out.println(year);
    }

    /**
     * ZoneId dan ZoneOffset merupakan timezone versi terbaru
     */
    @Test
    void zoneIDAndOffset() {
        //ZoneId
        ZoneId zoneId = ZoneId.systemDefault();
        ZoneId zoneGmt = ZoneId.of("GMT"); //Asia/Jakarta and etc..
        Set<String> availableZoneId = ZoneId.getAvailableZoneIds(); //list zpne id yang didukung

        availableZoneId.forEach(System.out::println);
        System.out.println(zoneGmt);
        System.out.println(zoneId);

        //ZoneOffset ini berkaitan GMT+0, GMT+7 indonesa and etc...
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00"); //di set ke gmt+7(indoneisa)
        ZoneOffset zoneOffset1 = ZoneOffset.ofHours(-7); //di set minus 7 atau ke gmt+0

        System.out.println(zoneOffset1);
        System.out.println(zoneOffset);
    }

    /**
     * zoneDateTime seperti localDateTime yang membedakan untuk zoneDateTime bisa untuk format locationnya
     * wajib menggunakan zoneId or zoneOffset
     *
     * turunan dari ZoneDateTime = offsetTime dan offsetDateTime
     * dibagian kelas turunan ini tidak bisa set zone by id tapi yang nanti tamppin hanya offsetTime  saja +07:00, +05:00 etc..
     * untuk konver localtime = offsetTime || localDateTime = OffsetDateTime etc..
     * begitujuga bisa konversi ke zoneDateTime
     */
    @Test
    void zonedDateTime() {
        ZonedDateTime zonedDateTime = ZonedDateTime.now(); //initialization waktu dan location time saat ini
        ZonedDateTime zonedDateTime1 = ZonedDateTime.of(LocalDateTime.now(), ZoneId.of("Asia/Jakarta")); //set zonedate ke asia jakarta
        ZonedDateTime zonedDateTime2 = ZonedDateTime.now(ZoneId.of("GMT")); //set zonedate ke gmt+0
        ZonedDateTime zonedDateTime3 = ZonedDateTime.now(ZoneOffset.ofHours(5)); //set zonedate ke gmt+5


        //conversion
        ZonedDateTime zonedDateTime4 = ZonedDateTime.parse("2021-05-12T15:55:54+07:00");//07:00 bisa dirubah ke [Asia/Jakarta]

        //class unik yang tidak ada di callendar
        ZonedDateTime zonedDateTime5 = zonedDateTime.withZoneSameLocal(ZoneId.of("GMT")); //ini hanya akan merubah gmtnya saja

        System.out.println(zonedDateTime5);
        System.out.println(zonedDateTime4);
        System.out.println(zonedDateTime3);
        System.out.println(zonedDateTime2);
        System.out.println(zonedDateTime1);
        System.out.println(zonedDateTime);
    }

    /**
     * miliseccond id representasikan dengan nama instant di java date and time terbaru
     */
    @Test
    void instantTest() {
        Instant instant = Instant.now();
        Instant instant1 = Instant.ofEpochMilli(0); //untuk menempatkan milisecond yang akan diubah ke date

        //modification
        Instant instant2 = instant.plusSeconds(4000); //menaikkan 4000 detik

        //conversion
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant,ZoneId.of("Asia/Jakarta")); //conversi instant ke localdatetime
        //ini juga bisa digunakan untuk Zonedatetime beserta turunanya

        //yang bisa dijadikan conversi ke instant jika datetime itu lengkap (LocalDateTime,ZoneDateTime,offSetDateTime)
        Instant instant3 = LocalDateTime.now().toInstant(ZoneOffset.ofHours(7));
        Instant instant4 = ZonedDateTime.now().toInstant();

        System.out.println("zonedatetime to instant :"+instant4);
        System.out.println("Localdatetime to instant :"+instant3);
        System.out.println(localDateTime);
        System.out.println("second :"+instant.getEpochSecond());
        System.out.println("epoch :"+instant.toEpochMilli());
        System.out.println(instant2);
        System.out.println(instant1);
        System.out.println(instant);
    }

    /**
     * Clock ini merepresentasikan tanggal dan waktu saat ini
     * clock juga menjadi best praktis  dan bisa di convert kemanapun
     */
    @Test
    void clockTest() {
       Clock clock = Clock.systemUTC();
       Clock clock1 = Clock.systemDefaultZone(); //akan menggunakan bawaan yang di laptop atau server
       Clock clock2 = Clock.system(ZoneId.of("Asia/Jakarta"));

       //get data in clock
        Instant instant = clock1.instant(); //get data in clock conver to instant
        ZonedDateTime zonedDateTime = ZonedDateTime.now(clock1);
        YearMonth yearMonth = YearMonth.now(clock1);
        MonthDay monthDay = MonthDay.now(clock1);
        LocalTime localTime = LocalTime.now(clock1);
        LocalDateTime localDateTime = LocalDateTime.now(clock1);
        OffsetTime offsetTime = OffsetTime.now(clock1);
        OffsetDateTime offsetDateTime = OffsetDateTime.now(clock1);


        //yang membuktikan bahwa clock bisa di conver kemana aja
        System.out.println("offsetdateTime :"+offsetDateTime);
        System.out.println("offsettime :"+offsetTime);
        System.out.println("Local date Time :"+localDateTime);
        System.out.println("local time :"+ localTime);
        System.out.println("mont day :"+ monthDay);
        System.out.println("year month :"+yearMonth);
        System.out.println(zonedDateTime);//2021-08-17T19:11:24.169939+07:00[Asia/Jakarta] ini sesuai timezone clock
        System.out.println(instant);//2021-08-17T12:07:24.211735Z  minus karena instant tidak memiliki timezone sehingga menggunakan default gmt
        System.out.println(clock2);//SystemClock[Asia/Jakarta
        System.out.println(clock1);//SystemClock[Asia/Jakarta
        System.out.println(clock);
    }

    /**
     * Duration berguna untuk mengkonversi ke waktu yang di inginkan
     * berguna juga untuk mengukur durasi dari 2 datetime
     */
    @Test
    void durationTest() {
        Duration duration = Duration.ofDays(1); //duration saya buat 1 hari

        //between duration
        Duration duration1 = Duration.between(LocalDateTime.now(), LocalDateTime.now().plusHours(1)); //betwen 2 date time untuk mendapatkan durasi

        System.out.println(duration1);//PT1H0.000102S  ada milisecond karena beda waktu generate time
        System.out.println(duration.toNanos());
        System.out.println(duration.toMillis());
        System.out.println(duration.toMinutes());
        System.out.println(duration.toHours()); //convert duration day ke hours
        System.out.println(duration);
    }

    /**
     * untuk period ini sama dengan durarion
     * tapi untuk period in fokus ke tanggal aja tapi tidak bisa di conver
     */
    @Test
    void periodTest() {
        Period period = Period.ofYears(1); //set period 1 tahun
        Period period1 = Period.of(10,4,3);

        //between
        Period period2 = Period.between(
                LocalDate.of(1990, 10,12),
                LocalDate.now()
        );


        System.out.println(period2.getYears()); //untuk mendapatkan perbedaan tahu antara sekarang dan yang di set
        System.out.println(period1.getDays());//get hari dari period
        System.out.println(period1.getMonths());//get nilai month
        System.out.println(period1.getYears()); //get nilai year
        System.out.println(period);
    }

    /**
     * temporal adalah paren dari LocalDateTime, zonedatetime dan di bawahnya
     */
    @Test
    void temporalTest() {
        Temporal temporal = LocalDateTime.now();
        Temporal temporal1 = ZonedDateTime.now();

        Temporal temporal2 = temporal1.plus(Duration.ofHours(2)); //tempora amount yang dimana bisa di tambah atau minuskan

        //temporal unit
        long temporalUnit= ChronoUnit.MINUTES.between(LocalDateTime.now(), LocalDateTime.now().plusHours(2));

        //temporal field
        Integer temporalField = temporal.get(ChronoField.YEAR); //untuk mendapatkan year dan masih banyak lagi


        //temporal query
        List<Integer> integers = temporal.query((TemporalQuery<? extends List<Integer>>) temporals ->{
           ArrayList<Integer> list = new ArrayList<>();
           list.add(temporals.get(ChronoField.YEAR));
            list.add(temporals.get(ChronoField.MONTH_OF_YEAR));
            list.add(temporals.get(ChronoField.DAY_OF_MONTH));

            return list;
        });

        //temporalAdjuster untuk melalukan perubaha tanggal
        Temporal temporal3 = temporal.with(TemporalAdjusters.firstDayOfMonth()); //merubah tanggah ke tanggal 1


        System.out.println(temporal3);
        System.out.println(integers);
        System.out.println(temporalField);
        System.out.println(temporalUnit);//120 karena ini membedakan betwen berdasarkan menit
        System.out.println(temporal2);
        System.out.println(temporal1);
        System.out.println(temporal);
    }

    @Test
    void dayOfWeekTest() {
        DayOfWeek dayOfWeek = DayOfWeek.TUESDAY;
        DayOfWeek dayOfWeek1 = dayOfWeek.plus(1);

        System.out.println(dayOfWeek1);
        System.out.println(dayOfWeek);
    }

    @Test
    void localDateFormatter() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd"); //format yang di inginkan
        LocalDate localDate = LocalDate.parse("2021 08 17", formatter);//data string

        //parsing
        LocalDate localDate1 = LocalDate.now();
        String formatterParsing = localDate1.format(formatter);

        //internationalization
        Locale locale = new Locale("id","ID");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy MMMM dd", locale); //set datetime zone indonesia
        String formatInternationalization = localDate1.format(dateTimeFormatter);

        System.out.println(formatInternationalization);
        System.out.println(formatterParsing);
        System.out.println(localDate);
    }

    /**
     * legacy berguna untuk konversi datetime lama ke date time jenis baru dan sebaliknya
     * konversi yang dibutuhkan menggunakan instan
     */
    @Test
    void legacyTest() {
        Date date = new Date(); //type data yang dulu
        Instant instant = date.toInstant(); //dirubah dulu ke instance supaya bisa rubah kemana mana

        Calendar calendar = Calendar.getInstance();
        Instant instant1 = calendar.toInstant();

        TimeZone timeZone = TimeZone.getDefault();
        ZoneId zoneId = timeZone.toZoneId();//konversi Timezone lama ke zone

        //kebalikan dari terbaru ke lama
        Date date1 = Date.from(ZonedDateTime.now().toInstant()); //instante ke date lama
        Calendar calendar1 = GregorianCalendar.from(ZonedDateTime.now()); //date baru ke callendar lama
        TimeZone timeZone1 = TimeZone.getTimeZone(zoneId); //zone to timeZone

        System.out.println(timeZone1);
        System.out.println(calendar1);
        System.out.println(date1);
        System.out.println(zoneId);
        System.out.println(instant1);
        System.out.println(instant);
    }
}
