package com.anggit.oop;
/**
 * About OOp
 * - Object hasrus didalam class
 * - class bisa memiliki beberapa object
 * - object class motorCycle = Brand, Type, MadeIn
 * - access level
 *      public : class, package, subclass, another package
 *      protected: class, package, subclass
 *      private : class
 *      default: class, package
 */
public class OOP {
    public static void main(String[] args) {
//        aboutConstructor();

        /**
         * Mewariskan dari parent ke child
         * Manager to Staff
         */
//        inheritance();

        /**
         * Berkaitan errat dengan inheritance
         * Tujuan untuk melakukan perubahan bentuk
         */
        polymorphism();


    }

    public static void aboutConstructor(){
        MotorCycle motorCycle = new MotorCycle();
        motorCycle.brand="Honda";
        motorCycle.type="Matic";
        motorCycle.myBrand("Rita");

        MotorCycleConstructor motorCycleConstructor = new MotorCycleConstructor("Suzuki");
        motorCycleConstructor.myBrand("Dedit");
    }

    public static void inheritance(){
        //Parent
        Manager manager = new Manager();
        manager.job="handle staff";
        manager.showName("Agus");

        //Child
        Staff staff = new Staff();
        staff.job = "Execute task from Manager";
        staff.showName("Anggi");

        //supper constructor
        StaffConstructor staffConstructor = new StaffConstructor("Support Manager");
        staffConstructor.showName("Angga");
    }

    public static void polymorphism(){
        //Manager
        Manager manager = new Manager();
        manager.job="Handling project";
        manager.showName("Udin");

        //Manager accept Start
        manager = new Staff();
        manager.job="Accept job from Manager";
        manager.showName("Dwi");
    }
}
