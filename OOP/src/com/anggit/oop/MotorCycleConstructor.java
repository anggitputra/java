package com.anggit.oop;

public class MotorCycleConstructor {
    String brand; //default null

    /**
     * Constructor
     * @param brandName
     * note:
     * contructor bisa dibuat overloading
     * constructor representasi class
     */
    public MotorCycleConstructor(String brandName) {
        //please don't same variable name
        brand = brandName;
        //handling shadowing variable
//        this.brand = brandName;
    }


    //call constructor with 1 parameter
    public  MotorCycleConstructor(){
        this(null);
    }

    //method
    void myBrand(String name){
        System.out.println("My Name: "+name +" brand my motor: "+brand);
    }

}
