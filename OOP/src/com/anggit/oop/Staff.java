package com.anggit.oop;

public class Staff extends Manager{

    //overriding method for parent /replace
    void showName(String name){
        System.out.println("My name "+ name + " jobs staff "+ this.job);
        //supper always access parent
        System.out.println("My name "+ name + " jobs supper staff "+ super.job);
    }
}
