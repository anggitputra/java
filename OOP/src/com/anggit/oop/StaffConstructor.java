package com.anggit.oop;

public class StaffConstructor extends ManagerConstructor{
    //supper representation get from parent object
    public StaffConstructor(String job) {
        super(job);
    }
    void showName(String name){
        System.out.println("My name "+ name + " jobs "+ super.job);
    }
}
