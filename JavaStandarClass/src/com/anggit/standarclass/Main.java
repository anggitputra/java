package com.anggit.standarclass;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        /**
         * Java standar calses
         * bisa akses di broses sesuai dengan java version yang di gunakan
         * disini meneggunakan java 1 = https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/package-summary.html
         * java class sangat banyak sekali dan sangat berguna untuk meringakan pekerjaan
         */
        stringClass();
        dateClass();
        systemClass();
        uuidClass();
        base64Class();
        try {
            propertiesClass();
        }catch (Exception e){
            System.out.println("Error Read env: "+ e.getMessage());
        }
        arrayClass();
        regexClass();
    }

    public static void stringClass(){
        String name = "Gabriel Anggraini";
        String uppercase = name.toUpperCase();
        System.out.println("Uppercase =>"+ uppercase);
        Integer length = name.length();
        System.out.println("Name length :"+length);
        String[] array = name.split(" ");
        System.out.println("Array String ="+Arrays.toString(array));

        //String Buffer bisa di akses secara paralel
        StringBuffer cityName = new StringBuffer();
        cityName.append("Bandung");
        cityName.append(" ");
        cityName.append("Wes Java");
        System.out.println("City Name =>"+ cityName);

        //String Builder
        StringBuilder employeeName = new StringBuilder();
        employeeName.append("Suhardi");
        employeeName.append(" ");
        employeeName.append("Susanto");
        System.out.println("Nama panjang =>"+ employeeName.toString());
    }

    public static void dateClass(){
        //sudah banyak yang deprecated
        Date date = new Date();
        System.out.println("Date = "+ date);

        //callendar yang paling di support saat ini
        Calendar thisDay = Calendar.getInstance();
        thisDay.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        System.out.println("This Day = "+ thisDay.getTime());
    }

    public static void systemClass(){
        //akan mengambil dari env system
        String language = System.getenv("LANGUAGE");
        System.out.println("Language Env ="+language);
    }

    public static void uuidClass(){
        //unique dan takkan pernah sama
        UUID uuid = UUID.randomUUID();
        System.out.println("UUID ="+ uuid);
    }

    public static void base64Class(){
        String password = "h4324";
        String passwordEncode = Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
        System.out.println("Password Encode ="+ passwordEncode);
        String passwordDecode = new String(Base64.getDecoder().decode(passwordEncode));
        System.out.println("Password Decode = "+passwordDecode);
    }

    public static void propertiesClass() throws IOException {
        //get value from properties
        //handling exception with try catch
        Properties properties = new Properties();
        properties.load(new FileInputStream("application.properties"));

        System.out.println("Db Password: " + properties.getProperty("db.password"));
        System.out.println("Db Username: "+ properties.getProperty("db.username"));
    }

    public static void arrayClass(){
        Integer[] listNumber = {1,2,5,6,3,8,9};
        //array sorting
        Arrays.sort(listNumber);
        System.out.println("List Number: "+ Arrays.toString(listNumber));
    }

    public static void regexClass(){
        String input = "Indonesia dan timurleste adalah bagian negara asia timur";
        String replace = "tengara";
        String regex = "timur";

        //patter sebagai seet pola pencarian
        Pattern pattern = Pattern.compile(regex);
        //matcher untuk mencari kata dari string berdasar pattern
        Matcher matcher = pattern.matcher(input);
        input = matcher.replaceAll(replace);
        System.out.println("Result Indonesia = "+ input);
    }

}
