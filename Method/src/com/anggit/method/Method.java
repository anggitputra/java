package com.anggit.method;

public class Method {
    public static void main(String[] args) {
        // array with [] / ...
        Integer[] number = {1,2,3,4,5};
        System.out.println("Counter =>"+counter(number));
        System.out.println("Counters =>"+counters(1,2,3,4,5));

        //method overloading same name method with not same parameter
        printing("Budi");
        printing("Budi", 19);

        //recursive only call himself
        //hati hati dengan perulangan recursiv yang banyak karena membutuhkan memori dan menyebabkan error
        System.out.println("Recursive =>" + recursive(3));
    }

    static int counter(Integer[] number){
        Integer res = 0;
        for (Integer i: number) {
            res += i;
        }
        return res;
    }

    static int counters(Integer... number){
        Integer res = 0;
        for (Integer i: number) {
            res += i;
        }
        return res;
    }

    static void printing(String name){
        System.out.println("Print Name: "+ name);
    }

    static void printing(String name, Integer age){
        System.out.println("Print Name: "+ name + " Age: "+ age);
    }

    static int recursive(Integer data){
        if (data == 1){
            return 1;
        }else {
            return data * recursive(--data);
        }
    }

}
