package com.anggit.javathread;

public class DaemonApp {
    public static void main(String[] args) {
        var thread = new Thread(()->{
            try {
                Thread.sleep(2_000L);
                System.out.println("Run Thread");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        thread.setDaemon(true);//ini berguna untuk menutup thread jika aplikasinya selesai
        thread.start();
    }
}
