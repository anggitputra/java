package com.anggit.javathread;

import java.util.concurrent.atomic.AtomicLong;

public class CounterAtomic {
    private AtomicLong value = new AtomicLong(0L); //atomic untuk menangani race condition

    public void increment(){
        value.incrementAndGet();
    }

    public Long getValue(){
        return value.get();
    }
}
