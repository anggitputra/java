package com.anggit.javathread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CounterReadWriteLock {
    private Long value = 0L;
    final private ReadWriteLock lock = new ReentrantReadWriteLock();

    //ini untuk locking bagian write
    public void increment(){
        try {
            lock.writeLock().lock();//dibuat lock disini
            value++;
        }finally {
            lock.writeLock().unlock();//di unlock ketika selesai
        }
    }

    //ini locking bagian read
    public Long getValue(){
        try {
            lock.readLock().lock();
            return value;
        }finally {
            lock.readLock().unlock();
        }
    }
}
