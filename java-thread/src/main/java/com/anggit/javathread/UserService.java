package com.anggit.javathread;

public class UserService {
    final ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public void setUser(String user){
        threadLocal.set(user); //thread ini akan menyimpan di thread local dan hanya bia di acces thread yang sama
    }

    public void doAction(){
        var user = threadLocal.get();//ini diakses dengan thread yang sama
        System.out.println(user + "Do actin");
    }
}
