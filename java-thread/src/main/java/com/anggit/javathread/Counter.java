package com.anggit.javathread;

public class Counter {
    private Long value = 0L;

    public void increment(){
//    public synchronized void increment(){ //ketika method di tambahkan synchronized maka otomatis methos itu hanya bisa di akses 1 method dan tidak bisa berbarengan
//        value++;
        synchronized (this){ //tehnik lock dengan synchronized statement dan ini lebih cepat dari synchronizedmet method karena lebih kecil skalanya
            value++;
        }
    }

    public Long getValue(){
        return value;
    }
}
