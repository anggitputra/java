package com.anggit.javathread;

import java.io.StringReader;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CoutnerLock {
    private Long value = 0L;
    final private Lock lock = new ReentrantLock(); //dibuat final supaya tidak berubah

    public void increment(){
        try {
            lock.lock();//dibuat lock disini
            value++;
        }finally {
            lock.unlock();//di unlock ketika selesai
        }
    }

    public Long getValue(){
        return value;
    }
}
