package com.anggit.javathread;

import org.springframework.boot.SpringApplication;

/**
 * thread adalah proses yang ringan
 * secara default program java running hanya di thread main
 *
 */
public class MainApplication {
    public static void main(String[] args) {
       String threadName = Thread.currentThread().getName();
        System.out.println("Use thread = "+ threadName);
    }
}
