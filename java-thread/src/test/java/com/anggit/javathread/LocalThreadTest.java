package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * thread local untuk menyimpan data
 * dan hanya bisa digunakan hanya thread tersebut
 * contoh :
 *  - database transactional
 *  -
 */
public class LocalThreadTest {
    @Test
    void threadLocalTest() throws InterruptedException {
        final var random = new Random();
        final var userService = new UserService();
        final var executor = Executors.newFixedThreadPool(10);

        for (int i = 0; i<50; i++){
            final var index = i;
            executor.execute(() -> { //disini di implementasi pembuatan thread langsung set dan do action
                try {
                    userService.setUser("User "+ index);
                    Thread.sleep(1000+ random.nextInt(3000));
                    userService.doAction();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    /**
     * untuk generate angka random
     * dan akan membuat object beda beda
     * sehingga tiap thread memiliki object random beda beda
     */
    @Test
    void threadLocalRandom() throws InterruptedException {
        final var executor = Executors.newFixedThreadPool(100);

        for (int i = 0; i<100; i++){
            executor.execute(() -> {
                try {
                    Thread.sleep(1000);
                    var number = ThreadLocalRandom.current().nextInt(); //async generate random
                    System.out.println(number);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.awaitTermination(14, TimeUnit.SECONDS);
    }

    /**
     * ThreadLocalRandomStream untuk membuat random number secara stream
     */
    @Test
    void ThreadLocalRandomStream() throws InterruptedException {
        final var executor= Executors.newFixedThreadPool(10);
        executor.execute(() ->{
            ThreadLocalRandom.current().ints(1000, 0, 1000)
                    .forEach(System.out::println);
        });

        executor.shutdown();
        executor.awaitTermination(20,TimeUnit.SECONDS);
    }
}
