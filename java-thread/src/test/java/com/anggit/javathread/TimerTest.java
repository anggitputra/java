package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TimerTest {

    /**
     * timer ini bisa di implement di dalam cronjob
     * @throws InterruptedException
     */
    @Test
    void delayJobTest() throws InterruptedException {
        var task = new TimerTask(){ //menggunakan timerTask
            @Override
            public void run() {
                System.out.println("Delay jobs");
            }
        };

        var timer = new Timer(); //timer untuk menentukan skejule menjalankan timer task
        timer.schedule(task, 2000); //setelah 2 detik akan menjalankan task
        timer.schedule(task, 2000,2000); //setelah 2 detik akan menjalankan task dan setelah 2 detik lagi akan di run lagi

        Thread.sleep(10000L); // ini hanya untuk memancing karena tidak pake slep maka tidak akan muncuk karena thread main tidak menunggu kalo tidak di seting
    }


    /**
     * schedule executor service
     * sebagai pengganti timer karena saat ini yang paling di sarankan
     */
    @Test
    void scheduleTest() throws InterruptedException {
        var executor = Executors.newScheduledThreadPool(10);
//        var future = executor.schedule(() -> System.out.println("Hallo world"),5, TimeUnit.SECONDS); //akan menjalankan program setelah 5 second
        var future = executor.scheduleAtFixedRate(() -> System.out.println("Hallo world"),2,2, TimeUnit.SECONDS); //periodik dimana akan berjalan setelah 2 second dan berjarak 2 second
        System.out.println(future.getDelay(TimeUnit.SECONDS));
        executor.awaitTermination(1, TimeUnit.HOURS);
    }
}
