package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Handling councurrency dengan map
 * membuat map aman diakses jika dilakukan multiple get
 * karena sudah di desain thread save
 */
public class ConcurrentHashMapTest {
    @Test
    void countDownLatch() throws InterruptedException {
        final var countDown = new CountDownLatch(100);
        final var map = new ConcurrentHashMap<Integer, String>();
        final var executor = Executors.newFixedThreadPool(100);

        for (int i=0; i<100; i++){
            final var index = i;
            executor.execute(() ->{
                try {
                    Thread.sleep(1000);
                    map.putIfAbsent(index, "Data-"+ index);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    countDown.countDown();
                }
            });
        }

        executor.execute(() -> {
            try {
                countDown.await();
                map.forEach((integer, s) -> System.out.println(integer + ":"+ s));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    @Test
    void collectionTest() {
        List<String> list = List.of("Budi", "Badu", "Bayu", "Beni");
        List<String> synchronizedList = Collections.synchronizedList(list);
        synchronizedList.forEach(System.out::println);
    }
}
