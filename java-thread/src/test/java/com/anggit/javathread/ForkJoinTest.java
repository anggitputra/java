package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * fork join untuk mempermudah pekerjaan yang banyak dengan cara
 * eksekusi dengan membagi kemudian dan setelah ada hasil baru digabungin
 */
public class ForkJoinTest {
    @Test
    void forkJoinTest() {
        var forkJoin = ForkJoinPool.commonPool();//untuk memanfaatkan semua core
        var forkJoin1 = new ForkJoinPool(3);//menentukan core yang akan digunakan

        var executor = Executors.newWorkStealingPool();//untuk semua core
        var executor1 = Executors.newWorkStealingPool(3);
    }

    /**
     * fork join recursive action
     * ini bertujuan untuk tidak mengasih balikan atau void
     */
    public static class recursiveAction extends RecursiveAction{

        private List<Integer> integers;

        public recursiveAction(List<Integer> integers){
            this.integers = integers;
        }

        @Override
        protected void compute() {
            if (integers.size() <= 10){
                doExecute();
            }else {
                forkCompute();
            }
        }

        private void forkCompute(){
            List<Integer> integers = this.integers.subList(0, this.integers.size() / 2);
            List<Integer> integers1 = this.integers.subList(this.integers.size()/2, this.integers.size());

            recursiveAction task = new recursiveAction(integers);
            recursiveAction task1 = new recursiveAction(integers1);
            ForkJoinTask.invokeAll(task, task1);
        }

        private void doExecute(){
            integers.forEach(integer -> System.out.println(Thread.currentThread().getName()+" : "+integer));
        }
    }

    @Test
    void recursiveActionTest() throws InterruptedException {
        var pool = ForkJoinPool.commonPool(); //berjalan menggunakan 4 core di saya karena sesuai laptop dan pc masing masing
        List<Integer> integers = IntStream.range(0, 1000).boxed().collect(Collectors.toList());

        var task = new recursiveAction(integers);
        pool.execute(task);

        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.HOURS);
    }

    /**
     * fork join recursive task
     * ini fork join yang bisa kasih balikan value atau return
     */
    public static class TotalRecursiveTask extends RecursiveTask<Long>{
        private List<Integer> integers;

        public TotalRecursiveTask(List<Integer> integers) {
            this.integers = integers;
        }

        @Override
        protected Long compute() {
            if (integers.size() <= 10){
                return doCompute();
            }else{
                return forkCompute();
            }
        }

        private Long forkCompute() {
            List<Integer> integers = this.integers.subList(0, this.integers.size() / 2);
            List<Integer> integers1 = this.integers.subList(this.integers.size()/2, this.integers.size());

            TotalRecursiveTask task = new TotalRecursiveTask(integers);
            TotalRecursiveTask task1 = new TotalRecursiveTask(integers1);
            ForkJoinTask.invokeAll(task,task1);
            return task.join() + task1.join();//untuk menambahkan value
        }

        private Long doCompute() {
            return integers.stream().mapToLong(value -> value).peek(value -> {
                System.out.println(Thread.currentThread().getName() + " : "+value);
            }).sum();
        }
    }

    @Test
    void recursiveTaskTest() throws InterruptedException {
        var pool = ForkJoinPool.commonPool(); //berjalan menggunakan 4 core di saya karena sesuai laptop dan pc masing masing
        List<Integer> integers = IntStream.range(0, 1000).boxed().collect(Collectors.toList());

        TotalRecursiveTask task = new TotalRecursiveTask(integers);
        pool.submit(task);//di submit untuk mendapatkan return task
        Long total = task.join();
        System.out.println("Total keseluruhan ="+ total);
        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.HOURS);
    }

}
