package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.concurrent.*;

/**
 * Concurrent Collection
 * - BlockingQueue  =  dengan metode fifo
 *      - array = jika ada fixt data yang ditentukan
 *      - linked = jika ukuranya bisa berkembang (hati hati terhadap memory)
 *      - priority = berurutan berdasarkan prioritas tapi harus di set terlebih darhulu
 *      - delay = untuk melakukan data delay sampai datanya ada
 *      - synchronus = thread menambah data dan mengunggu sampai ada yang mengambil data
 * - ConcurrentMap = untuk concuren thread safe
 *
 * aman di akses untuk semua dan udah dijamin thread safe
 */
public class ConcurrentBlockingQueueTest {
    @Test
    void arrayLinkedTest() throws InterruptedException {
//        final var queue = new ArrayBlockingQueue<String>(5); //memberikan batasan 5 data array yang dapat di tampung
//        final var queue = new LinkedBlockingQueue<String>(); //akan menampung semua data yang dikirim
        final var queue = new LinkedBlockingDeque<String>(); //dequee untuk pengurutan lifo dand fifo
        final var executor = Executors.newFixedThreadPool(20);

        for (int i=0; i<10; i++){
            executor.execute(() -> {
                try
                {
//                    queue.put("Data"); //insert  || dan untuk put ini mengembalikan await
                    queue.putFirst("Data"); //deque
                    System.out.println("Finish Send Data");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
//                    var value = queue.take(); //untuk mengambil data dari queue
                    var value = queue.takeLast(); //deque
                    System.out.println("Recive data = " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    @Test
    void priorityTest() throws InterruptedException {
        //dengan priority ini nilai capacity dan dimasukkan juga yang menjadi priority
        final var queue = new PriorityBlockingQueue<Integer>(10, Comparator.reverseOrder()); //akan membalikkan data dari terbesar ke terkecil
        final var executor = Executors.newFixedThreadPool(20);

        for (int i=0; i<10; i++){
            final var index = i;
            executor.execute(() -> {
                queue.put(index);
                System.out.println("Finish Send Data");
            });
        }

        executor.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    var value = queue.take(); //untuk mengambil data dari queue
                    System.out.println("Recive data = " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    @Test
    void delayTest() throws InterruptedException {
        final var queue = new DelayQueue<ScheduledFuture<String>>();//disini untuk seting dimana delayque ini menggunakan scheduled future dan datanya string
        final var executor = Executors.newFixedThreadPool(20);
        final var executorScheduled = Executors.newScheduledThreadPool(10);

        for (int i=0; i<10; i++){
            final var index = i;
            queue.put(executorScheduled.schedule(() -> "Data "+ index, i, TimeUnit.SECONDS));
        }

        executor.execute(() -> {
            while (true) {
                try {
                    var value = queue.take();
                    System.out.println("Receive data = " + value.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    /**
     * data tidak berurutan
     * dan ketika ada que masuk langsung ada yang take
     * @throws InterruptedException
     */
    @Test
    void synchronousTest() throws InterruptedException {
        final var queue = new SynchronousQueue<String>();
        final var executor = Executors.newFixedThreadPool(20);

        for (int i=0; i<10; i++){
            final var index = i;
            executor.execute(() -> {
                try {
                    queue.put("Data "+ index);
                    System.out.println("Finish Put Data :"+ index);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    var value = queue.take(); //untuk mengambil data dari queue
                    System.out.println("Recive data = " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    /**
     * ini harus ada yang tranfer dan consum atau artinya one to one
     * kalo tidak maka akan menunggu sampai ada yang consume dan create
     * @throws InterruptedException
     */
    @Test
    void tranferLinkedTest() throws InterruptedException {
        final var queue = new LinkedTransferQueue<String>();
        final var executor = Executors.newFixedThreadPool(20);

        for (int i=0; i<10; i++){
            final var index = i;
            executor.execute(() -> {
                try
                {
                    queue.transfer("Data "+ index);
                    System.out.println("Finish Send Data :"+ index);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }

        executor.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    var value = queue.take();
                    System.out.println("Recive data = " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        executor.awaitTermination(1, TimeUnit.HOURS);
    }
}
