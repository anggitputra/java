package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.ReadWriteLock;

/**
 * race condition bisa di selesaikan dengan synchronitation
 * tapi ini dibuat blocking sehingga agar sedikit lambat
 * -synchronize method di method (dengan melakukan lock)
 * -synchronize statement
 */
public class RaceConditionTest {
    @Test
    void counter() throws InterruptedException {
        var counter = new Counter();
        Runnable runnable = () ->{
            for (int i = 0; i<20000L; i++){
                counter.increment();
            }
        };

        //hasil 27000 karena ini blocking
//        runnable.run();
//        runnable.run();
//        runnable.run();

        //dengan data counter 20 ribu menghasilkan 57415 ini ada data yang hilang dan ini yang disebutrace condition
        var thread = new Thread(runnable);
        var thread1 = new Thread(runnable);
        var thread2 = new Thread(runnable);
        thread.start();
        thread1.start();
        thread2.start();
        thread.join();
        thread1.join();
        thread2.join();

        System.out.println(counter.getValue());
    }

    /**
     * Atomic
     * untuk menangani race condition dan ini yang direkomendasikan saat ini
     * untuk object bisa menggunakan AtomicReference</>
     * untuk penggunaanya disesuaikan tipe datanya
     */
    @Test
    void counterAtomic() throws InterruptedException {
        var counter = new CounterAtomic(); //cukup memanggil clas atomic tadi dan tidak terjadi race condition
        Runnable runnable = () ->{
            for (int i = 0; i<20000L; i++){
                counter.increment();
            }
        };
        var thread = new Thread(runnable);
        var thread1 = new Thread(runnable);
        var thread2 = new Thread(runnable);
        thread.start();
        thread1.start();
        thread2.start();
        thread.join();
        thread1.join();
        thread2.join();

        System.out.println(counter.getValue());
    }

    /**
     * Locks
     * merupakan versi terbaru untuk melakukan locking di councurency
     * yang sebelumnya statement log dan method log dan in lebih mucah penggunaanya
     * jangan lupan gunakan finally di trycat untuk unlock untuk menangaru ketika terjadi catch
     */
    @Test
    void counterLock() throws InterruptedException {
        var counter = new CoutnerLock();
        Runnable runnable = () ->{
            for (int i = 0; i<20000L; i++){
                counter.increment();
            }
        };
        var thread = new Thread(runnable);
        var thread1 = new Thread(runnable);
        var thread2 = new Thread(runnable);
        thread.start();
        thread1.start();
        thread2.start();
        thread.join();
        thread1.join();
        thread2.join();

        System.out.println(counter.getValue());
    }

    /**
     * ReadWrite
     * berguna untuk mendapatkan read lock ataupu write lock
     * sehingga berhemad denagn readwrite lock tanpa harus membuat 2 buah lock
     */
    @Test
    void counterReadWriteLock() throws InterruptedException {
        var counter = new CounterReadWriteLock();
        Runnable runnable = () ->{
            for (int i = 0; i<20000L; i++){
                counter.increment();
            }
        };
        var thread = new Thread(runnable);
        var thread1 = new Thread(runnable);
        var thread2 = new Thread(runnable);
        thread.start();
        thread1.start();
        thread2.start();
        thread.join();
        thread1.join();
        thread2.join();

        System.out.println(counter.getValue());
    }
}
