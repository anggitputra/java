package com.anggit.javathread;

import org.junit.jupiter.api.Test;
import org.springframework.ui.context.Theme;

public class MainTest {

    @Test
    void threadTest() {
        //thread berada di kelas runnable
        Runnable runnable = () -> System.out.println("Thread Location = " + Thread.currentThread().getName());

        //untuk mendeklarasikan sebagai tread
        var thread = new Thread(runnable);
        thread.start();// untuk menjalankan thread

        System.out.println("Starting Program");
    }

    //thread slepp tidak cocok di implementaskan ketika multithread di buat blocking
    @Test
    void threadSleepTest() throws InterruptedException{
        Runnable runnable = () -> {
            try {
                Thread.sleep(2_000L); //mematikan thread selama 2 detik
                System.out.println("Thread Location = " + Thread.currentThread().getName());
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        };


        var thread = new Thread(runnable);
        thread.start();
        System.out.println("Starting Program");
        Thread.sleep(3_000L); //mematikan thread main selama 3 detik untuk menunggu thread runnable
    }

    @Test
    void threadJoinTest() throws InterruptedException{
        Runnable runnable = () -> {
            try {
                Thread.sleep(3_000L);
                System.out.println("Thread Location = " + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };


        var thread = new Thread(runnable);
        thread.start();
        System.out.println("Starting Program");
        thread.join();//thread ini akan mennggun sampai thread start selesai
        System.out.println("Program selesai");
    }

    @Test
    void threadInteruotTest() throws InterruptedException{
        Runnable runnable = () -> {
            for (int i = 0; i<10; i++){
                if (Thread.interrupted()){
                    return; //juga untuk metikan thread yang yang paling benar
                }
                System.out.println("Runnable "+i);
                System.out.println("Thread Location = " + Thread.currentThread().getName());
                System.out.println("Thread state = " + Thread.currentThread().getState());
//                try {
//                    Thread.sleep(2_000L);
//                    System.out.println("Thread Location = " + Thread.currentThread().getName());
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
////                    return;//untuk menghentikan looping thread
//                }
            }
        };

        var thread = new Thread(runnable);
        thread.setName("Tread Counter");
        thread.start();
        System.out.println(thread.getState());//New ini untuk mengetahui tread sedang apa
        Thread.sleep(2_000L);
        System.out.println("Starting Program");
        thread.interrupt();//untuk menghentikan thread yang berjalan yang melebihi 5 second
        thread.join();
        System.out.println(thread.getState());//TERMINATED
        System.out.println("Program selesai");
    }
}
