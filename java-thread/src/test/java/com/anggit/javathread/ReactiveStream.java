package com.anggit.javathread;

import net.bytebuddy.description.field.FieldList;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.concurrent.TimeUnit;

/**
 * reactive stream yang memfokuskan pada aliran data
 * untuk skema subcriber mirip queue dimana disini untunk menampung menggunakan buffer
 * untuk default buffer 256  sehinggal ketika melewati itu akan atri
 * tapi ini juga bisa diubah
 */
public class ReactiveStream {

    public static class PrintSubscriber implements Flow.Subscriber<String>{

        private Flow.Subscription subscription;

        //untuk menarik data
        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            this.subscription = subscription;
            this.subscription.request(1);
        }

        // menerima permintaan dari on subcribe
        @Override
        public void onNext(String item) {
            try {
                Thread.sleep(1000);
                System.out.println(Thread.currentThread().getName()+" : "+ item);
                this.subscription.request(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //handling error
        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }

        //handling complet
        @Override
        public void onComplete() {
            System.out.println(Thread.currentThread().getName() + ": Done");
        }
    }

    @Test
    void publishTest() throws InterruptedException {
//        var publiser = new SubmissionPublisher<String>(Executors.newWorkStealingPool(10), 50); //ini untuk merubah daya tampung buffer
        var publiser = new SubmissionPublisher<String>();
        var subscriber = new PrintSubscriber();
        publiser.subscribe(subscriber); //punliser yang akan mengirim ke subcriber

        var executor = Executors.newWorkStealingPool(10);
        executor.execute(() -> {
            for (int i = 0; i< 100; i++){
                publiser.submit("Anggit :" + i);
                System.out.println(Thread.currentThread().getName()+" Send Anggit "+ i);
            }
        });
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.HOURS);
        Thread.sleep(100*1000);
    }


    public static class HelloProcessor extends  SubmissionPublisher<String> implements Flow.Processor<String, String>{

        private Flow.Subscription subscription;

        @Override
        public void onSubscribe(Flow.Subscription subscription) {
            this.subscription = subscription;
            this.subscription.request(1); //data ini bisa berubah ubah tergatung mau di set berapa
        }

        @Override
        public void onNext(String s) {
            var value = "Hello "+ s;
            submit(value); //untuk mengirim
            this.subscription.request(1);
        }

        @Override
        public void onError(Throwable throwable) {
            throwable.printStackTrace();
        }

        @Override
        public void onComplete() {
            close();//harus di close untuk mengasih tahu telah selesai
        }
    }

    /**
     * Untuk processor ini berguna untuk merubah value yang dikirim dari subcribtion ke subcriber
     * @throws InterruptedException
     */
    @Test
    void processorTest() throws InterruptedException {
        var publisher = new SubmissionPublisher<String>();
        var processor = new HelloProcessor();
        publisher.subscribe(processor); //publiser mengirim ke processor

        var subcriber = new PrintSubscriber();
        processor.subscribe(subcriber); //juga mengambil dari processor

        var executor = Executors.newWorkStealingPool(10);
        executor.execute(() -> {
            for (int i = 0; i< 100; i++){
                publisher.submit("Anggit :" + i);
                System.out.println(Thread.currentThread().getName()+" Send Anggit "+ i);
            }
        });

        Thread.sleep(100*1000);
    }
}
