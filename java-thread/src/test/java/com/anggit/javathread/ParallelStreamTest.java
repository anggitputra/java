package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParallelStreamTest {

    /**
     * thread poll ini memaksimalkan kerja core yang ada
     * sehingga semua core akan kebagian tugas
     */
    @Test
    void parallel() {
        Stream<Integer> stream = IntStream.range(0, 1000).boxed();

        stream.parallel().forEach(integer -> { //untuk define menjadi async
            System.out.println(Thread.currentThread().getName() + ":" + integer);
        });
    }

    @Test
    void customPool() {
//        var pool = new ForkJoinPool(5);
        var pool = ForkJoinPool.commonPool();
        ForkJoinTask<?> task = pool.submit(() -> {
            Stream<Integer> stream = IntStream.range(0, 1000).boxed();
            stream.parallel().forEach(integer -> {
                System.out.println(Thread.currentThread().getName() + ":" + integer);
            });
        });

        task.join();
    }
}
