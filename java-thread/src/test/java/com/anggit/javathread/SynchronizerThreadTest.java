package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

/**
 * synchronizer
 * lebih highet daripada lock
 * class yang berada dibahwanya
 *  - Semaphore = untuk manage data counter
 *  - CountDownLatch
 *  - CyclicBarrier
 *  - Phaser
 *  - Exchanger
 *  note : dijamin tidak ada race condition
 */
public class SynchronizerThreadTest {

    /**
     * semaphore ini ketika thread penuh 10 makan akan menggu sampai ada yang kosong
     * dan jika ada yang kosong langusng di masukkan thread yang akan masuk
     * @throws InterruptedException
     */
    @Test
    void SemaphoreTest() throws InterruptedException {
        final var semaphore = new Semaphore(10);//batas thread yang berjalan diatur disini
        final var executor = Executors.newFixedThreadPool(100); //max thread akan naik dika thread yang lain udan penuh

        for (int i=0; i<100; i++){
            executor.execute(() -> {
                try {
//                    semaphore.acquire();
                    semaphore.acquire();
                    Thread.sleep(1000);
                    System.out.println("Finish "+Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    semaphore.release();
                }
            });
        }

        executor.awaitTermination(1, TimeUnit.HOURS);
    }

    /**
     * CountDownLatch
     * ini sama seperti semaphore yang menbedakan ialah thread yang menunggu
     * harus menunggu sampai batas counter yang ditentukan sudah 0 terlebih dahulu
     */
    @Test
    void CountDownLatchTest() throws InterruptedException {
        final var countDownLatch = new CountDownLatch(5);
        final var executor = Executors.newFixedThreadPool(10);

        for (int i=0; i<6; i++){
            executor.execute(() -> {
                try {
                    System.out.println("Start Task");
                    Thread.sleep(2000);
                    System.out.println("Finish Task");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    countDownLatch.countDown();
                }
            });
        }

        executor.execute(() -> {
            try {
                countDownLatch.await();
                System.out.println("Finish All Task");
                executor.shutdownNow();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * CyclicBarrier
     * ini merupakan kebalikan dari CountDownLatch
     * dimana ini akan menunggu batas tunggu thread jika sudah memenuhi yang menunggu akan di eksekusi
     *
     */
    @Test
    void cyclicBarrier() throws InterruptedException {
        final var cyclicBarrier = new CyclicBarrier(5);
        final var executor = Executors.newFixedThreadPool(10);

        for (int i=0; i<5; i++){
            executor.execute(() -> {
                try {
                    cyclicBarrier.await();
                    System.out.println("Finish");
                } catch (BrokenBarrierException | InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executor.awaitTermination(2,TimeUnit.MINUTES);
    }

    /**
     * Phaser digunakan ketika kita membuthkan yang data thread berubah ubah
     * merupkan gabungan dari CyclicBarrier dan CountDownLatch
     * ini yang paling mudah paling di implemet dari pada iplement satu satu seperti yang diatas
     */
    @Test
    void phaserTest() throws InterruptedException {
        final var phaser = new Phaser();
        phaser.bulkRegister(5); //menentukan 5 thread, jika belum terpenuhi maka akan nunggu
        final var executor = Executors.newFixedThreadPool(10);//jumlah kerjaan yang akan thread tanggani

        //memberkan jobs kepada pool CountDownLatch
        for (int i= 0; i<5; i++){
            executor.execute(() -> {
                try {
                    System.out.println("Start Task");
                    Thread.sleep(2000);
                    System.out.println("End Task");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    phaser.arrive();//untuk mengurangi (CountDownLatch)
                }
            });
        }
        executor.execute(() -> {
            phaser.arriveAndAwaitAdvance();//default 0 (CyclicBarrier)
//            phaser.awaitAdvance(0); //(CountDownLatch)menunggu sampai 0 jika uda makan memunculkan all task done
            System.out.println("All task done");
        });

        executor.awaitTermination(10, TimeUnit.SECONDS);
    }

    /**
     * exchanger
     * untuk melakukan BERTUKAR data antar thread
     * tapi exchanger perlu pertukaran data dan harus ada yang menerima
     */
    @Test
    void exchangeTest() throws InterruptedException {
        final var exchanger = new Exchanger<String>(); //sebagai penampung pertukaran data
        final var executor = Executors.newFixedThreadPool(10);

        executor.execute(() -> {
            try {
                System.out.println("Thread pertama");
                var result = exchanger.exchange("pertama"); //memasukkan ke excahnger
                System.out.println("Thread pertama recive :"+ result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executor.execute(() -> {
            try {
                System.out.println("Thread kedua");
                var result = exchanger.exchange("kedua"); //memasukkan ke exchanger
                System.out.println("Thread kedua recive :"+ result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        executor.awaitTermination(10, TimeUnit.SECONDS);
    }

}
