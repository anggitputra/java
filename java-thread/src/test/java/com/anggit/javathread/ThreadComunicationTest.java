package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.ReentrantLock;

public class ThreadComunicationTest {

    private String message = null;

    /**
     * manual waiting sampai thread di panggin menggunakan while
     * @throws InterruptedException
     */
    @Test
    void manual() throws InterruptedException{
        var thread1 = new Thread(()->{
            while (message == null){ //looping seiap waktu dan menaikkan cpu operation

            }
            System.out.println(message);
        });

        var thread2 = new Thread(()->{ //mengirim message
            message = "Anggit Test";
        });

        thread2.start();
        thread1.start();
        thread2.join();
        thread1.join();
    }

    @Test
    void waitNotify() throws InterruptedException{

        final var lock = new Object();

        var thread1 = new Thread(()->{
            synchronized (lock){ //membuka lock karena di kirim notify
                try {
                    lock.wait(); //menunggu sampai dapat notify
                    System.out.println(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        var thread3 = new Thread(()->{
            synchronized (lock){ //membuka lock karena di kirim notify
                try {
                    lock.wait(); //menunggu sampai dapat notify
                    System.out.println(message);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        var thread2 = new Thread(()->{
            synchronized (lock){
                message = "Anggit";
//                lock.notify(); //melakukan notifi bahwa siap mengirim message (28)
                lock.notifyAll(); //melakukan notifi kesemua yang thread yang waiting tapi yang dalam di urutkan paling atas
            }
        });

        //posisi juga menentuka karena thread 2 melakukan notify
        thread3.start();
        thread1.start();

        thread2.start();

        thread3.join();
        thread1.join();
        thread2.join();
    }

    /**
     * Condition
     * merupakan versi terbaru untuk notify, notifyall dan waith
     */

    @Test
    void conditionTest() throws InterruptedException {
        var lock = new ReentrantLock();
        var condition = lock.newCondition();

        var thread = new Thread(() -> {
           try {
               lock.lock();
               condition.await();
               System.out.println(message); //akan menampilkan message dari yang dikirim
           } catch (InterruptedException e) {
               e.printStackTrace();
           }finally {
               lock.unlock();
           }
        });

        var thread2 = new Thread(() -> {
           try {
               lock.lock();
               Thread.sleep(2000);
               message = "Signal Lock Anggit";
               condition.signal();//hanya akan mengirim 1 signal
//               condition.signalAll();//untuk mengirim ke semua yang sedang await
           } catch (InterruptedException e) {
               e.printStackTrace();
           } finally {
               lock.unlock();
           }
        });

        thread.start();
        thread2.start();
        thread.join();
        thread2.join();
    }
}
