package com.anggit.javathread;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * perlu di ingat di unit test tidak akan menunggu yang sebuah async method
 * core pool = minimal thread yang di buat
 * max pool = maximal thread harus melihat memory yang didukung karena 1 thread 512 kb - 1mb
 * keep alive time = berguna untuk menghapus thread jika tidak di gunakan
 * queue = menampung pekerjaan yang akan di kirim (kemudian thread akan mengambil)
 *
 * Runnable = ini untuk menguirim job ke thred tapi ini balikanya void
 * Collable = ini seperti runnable tapi akan mengambilan data lewat Future<T>
 *
 */
public class HighThreadTest {

    /**
     * thread di bawah ini cukup baik untuk digunakan untuk async
     * @throws InterruptedException
     */
    @Test
    void treadTest() throws InterruptedException {
        var minThread=5;
        var maxThread=10; //max thread akan di panggil jika min thread sudah penuh
        var alive = 1;
        var aliveTime = TimeUnit.MINUTES;
        var queue = new ArrayBlockingQueue<Runnable>(100); //kerjaan yang dapat di tampung que
        var rejectedHandler = new LogRejectedExecutionHandler();

//        var executor = new ThreadPoolExecutor(minThread,maxThread,alive,aliveTime,queue); //inisialisasi executor
        var executor = new ThreadPoolExecutor(minThread,maxThread,alive,aliveTime,queue, rejectedHandler);//untuk handling ketika job melebihi queu

        for (int i = 0; i<150; i++){ //ketika capacity que dan thread yang masu melebih makan akan error karena kena rijected
            final var task = i;
            Runnable runnable = ()->{
                try {
                    Thread.sleep(1000);
                    System.out.println("Task "+task+" Runnable from thread :"+Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            executor.execute(runnable);
        }
//        Thread.sleep(5000L); //untuk menggu thread jika hanya q thred
//        executor.shutdown(); //mematikan menunggu selesai yang didalam queue
//        executor.shutdownNow(); //untuk langsung mematikan
        executor.awaitTermination(1, TimeUnit.HOURS); //untuk membatasi batas tunggu thread
    }


    /**
     * untuk handling ketika job melebihi batas queue yang telah di tentukan
     */
    public static class LogRejectedExecutionHandler implements RejectedExecutionHandler{
        @Override
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            System.out.println("Task" + runnable + "is rejected");
        }
    }

    /**
     *
     * @throws InterruptedException
     */
    @Test
    void singleThreadExecutorServiceTest() throws InterruptedException {
//        var executor = Executors.newSingleThreadExecutor(); //akan membuat singgle thread executor
//        var executor = Executors.newFixedThreadPool(10); //manage 10 thread saja yang jalan walaupun banyak job masuk
        var executor = Executors.newCachedThreadPool();//ini akan memaksimalkan thread berdasakjan job tapi tidak ada batasan sehingga berbahaya


        for (int i=0; i<50; i++){
            executor.execute(()->{
                try {
                    Thread.sleep(1000);
                    System.out.println("Runnable in thread :"+ Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.HOURS);
    }


    //Callable untuk mengembalikan return thread
    @Test
    void singleThreadCallable() throws ExecutionException, InterruptedException {
        var executor = Executors.newSingleThreadExecutor();

        Callable<String> callable = ()->{
            Thread.sleep(1000L);
            return "Anggit";
        };

        Future<String> future = executor.submit(callable); //untuk memberikan job ke ececutor
//        future.cancel(true);//untuk cancle executor akan ada cancel exception kalo mau di tangkap
//        System.out.println("is canceled :"+future.isCancelled()); //untuk menangkap hasil cancel
        while (!future.isDone()){ //untuk mengetahui executer udah done atau belum
            System.out.println("waiting ...");
            Thread.sleep(200);
        }
        String value = future.get();//untuk mengambil nilai string
        System.out.println("Selesai future"+value);
    }


    /**
     * untuk mengirim job ke executor secara bersamaan
     * invoke all akan menunggu semua selesai
     *
     * invoke any akan mengambil yang paling cepat saja
     */
    @Test
    void invokeAllTest() throws InterruptedException, ExecutionException {
        var executor = Executors.newFixedThreadPool(10);

        List<Callable<String>> callables = IntStream.range(1,11).mapToObj(value ->(Callable<String>)()->{
            Thread.sleep(value* 200);
            return String.valueOf(value);
        }).collect(Collectors.toList());

//        var future = executor.invokeAll(callables); //akan mengirim semua callable ke executor
//        for (Future<String> stringFuture : future){ //untuk menampilkan dari hasil invoke
//            System.out.println(stringFuture.get());
//        }

        var result = executor.invokeAny(callables); //ini hanya akan menampilkan yang tercepat saja
        System.out.println(result); //dengan invoke any yang dibalikan langsung 1 value aja
    }


    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    public Future<String> getValue(){
        CompletableFuture<String> future = new CompletableFuture<>();//initialisasi completable dengna mengebalikan string
        executorService.execute(()->{
            try {
                Thread.sleep(1000);
                future.complete("Selesai Processing");//ini berguna untuk menghetikan dan mengasih tahu bahwa porses telah complet
            } catch (InterruptedException e) {
                future.completeExceptionally(e);//akan mengirim error jika proses thread terdapat error
            }
        });
        return future;
    }

    /**
     * completable merupakan versi terbaru daripada callable ataupun runnable
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    void completableTest() throws ExecutionException, InterruptedException {
        Future<String> future = getValue(); //untuk mendapatkan value
        System.out.println(future.get());
    }

    private void execute(CompletableFuture<String> future, String value){
        executorService.execute(()->{
            try {
                Thread.sleep((long) (1000 + Math.random()));
                future.complete(value);
            } catch (InterruptedException e) {
                future.completeExceptionally(e);
            }
        });
    }

    @Test
    void completable2Test() throws ExecutionException, InterruptedException {
        CompletableFuture<String> future = new CompletableFuture<>();
        execute(future,"Thread 1");
        execute(future,"Thread 2");
        execute(future,"Thread 3");

//        System.out.println("Paling Cepat :"+ future.get());

        CompletableFuture<String[]> future1 = future //uini untuk melakukan perubahan nilai yang didapat dari thread tercepat
                .thenApply(string -> string.toUpperCase()) //dibuat ke uppercasre
                .thenApply(string -> string.split(" ")); //di split

        String[] strings = future1.get();
        for (var value : strings){
            System.out.println(value);
        }
    }


    //Completion Service
    private Random random = new Random();

    /**
     * Completion service ini betujuan sebagai jembatan antar thread sehingga
     * dapat menentukan tread mana yang akan mengambil dan memasukkan
     * @throws InterruptedException
     */
    @Test
    void completionTest() throws InterruptedException {
        var executor = Executors.newFixedThreadPool(5);
        var completionService = new ExecutorCompletionService<String>(executor);

        //thread yang akan di submit berjumlah 100
        Executors.newSingleThreadExecutor().execute(()->{
            for (int i = 0; i<100; i++){
                final var index = i;
                completionService.submit(()->{//submit ke completion service
                    Thread.sleep(random.nextInt(1000));
                    return "Task"+ index;
                });
            }
        });

        //mengambil thread dari completion service
        Executors.newSingleThreadExecutor().execute(()->{
            while (true){
                try {
                    Future<String> future = completionService.poll(5, TimeUnit.SECONDS);
                    if (future == null){
                        break;
                    }else {
                        System.out.println(future.get());
                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    break;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    break;
                }
            }
        });

        executor.awaitTermination(2, TimeUnit.SECONDS);//ini akan mennunggu sampai 2 second
    }
}
