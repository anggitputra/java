package com.anggit.learning;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * target ini bisa untuk semua elemet tapi disini untuk file saja
 * runtime supaya bisa dibaca dengan reflection
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotBlank {
}
