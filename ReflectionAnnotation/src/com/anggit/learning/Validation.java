package com.anggit.learning;

import java.lang.reflect.Field;

public class Validation {
    /**
     * Reflection disini untuk menamkap annotation
     * @param object
     */
    public static void validationReflection(Object object) {
        Class aClass = object.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (var field : fields) {
            field.setAccessible(true);

            if (field.getAnnotation(NotBlank.class) != null) {
                try {
                    String value = (String) field.get(object);
                    //condition yang diinginkan
                    if (value == null || value.isBlank()) {
                        //response yang di ingingkan
                        throw new BlankException("Field " + field.getName() + " is blank ");
                    }

                } catch (IllegalAccessException exception) {
                    System.out.println("Can't access field " + field.getName());
                }
            }

        }
    }

}
