package com.anggit.ShowInputOutput;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ShowInputOutput {
    public static void main(String[] args) {
//        scanner(); //Class scanner

//    public static void main(String[] args) throws Exception{
//        bufferReader();


        //output
        //start
        System.out.println("Good day");
        System.out.println("Beautiful place");

        System.out.print("Good day");
        System.out.print("Beautiful place");

        System.out.printf("Good day");
        System.out.printf("Beautiful place");
        /*
            output :
            Good day
            Beautiful place

            Good dayBeautiful placeGood dayBeautiful
        */
        //end

        long n = 461012;
        System.out.format("%d%n", n);      //  -->  "461012"
        System.out.format("%08d%n", n);    //  -->  "00461012"
        System.out.format("%+8d%n", n);    //  -->  " +461012"
        System.out.format("%,8d%n", n);    // -->  " 461,012"

//        note : all documentation about formatting numeric print output
//        https://docs.oracle.com/javase/tutorial/java/data/numberformat.html

    }

    //public access
    //void no return method
    //do not have to be called  first
    public static void scanner(){
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Add Name");
        String name = keyboard.nextLine();  //String
        System.out.println("Name: "+name);
        System.out.print("Add Age");
        Integer age = keyboard.nextInt();  //Integer
        System.out.println("Age: "+age);
        System.out.print("Add Phi");
        Float phi = keyboard.nextFloat(); //float
        System.out.println("Phi: "+phi);

       /*
       Note : Scanner support all type data (String, Integer, Float, Boolean, char, double)
        result :
            Anggit
            Name: Anggit
            19
            Age: 19
            3.14
            Phi: 3.14
        */
    }

    //must use throws Exception
    public static void bufferReader() throws Exception{
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Insert your Name: ");
        String name = br.readLine();
        System.out.println("Name: "+name);
        Boolean success = br.ready();
        System.out.println("Success: "+success);

        /*
            note :
            - support Integer, Boolean and String
            - InputStreamReader can't stand alone, always need BufferReader and Io Exception

            Insert your Name: ilham
            Name:  ilham
            Success: True
        */
    }

    //class console
    //I don't use it because it only work in the terminal

}
