package com.anggit.internationalization;

import org.junit.jupiter.api.Test;

import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Konsep internationalization secara naturan tidak ada untuk message
 * tapi ini dibantu dengan resource bundle
 */
public class Message {

    @Test
    public void messageTest() {
        var local = new Locale("in", "ID");
        var resourceBundle = ResourceBundle.getBundle("message", local);

        var pattern = resourceBundle.getString("wellcome.message");//mengambil value dari properties
        var messageFormat = new MessageFormat(pattern);
        var format = messageFormat.format(new Object[]{
                "Anang","Handanu" //sett value yang digunakan di message dimana terdapat 2 value
        });
        System.out.println(format);
    }

    /**
     * message format type ini berguna untuk memformat message sesuai yang di inginkan
     * doc : https://docs.oracle.com/javase/8/docs/api/java/text/MessageFormat.html
     */
    @Test
    public void messageFormatTest(){
        var local = new Locale("in", "ID");
        var resourceBundle = ResourceBundle.getBundle("message", local);

        var pattern = resourceBundle.getString("status");
        var messageFormat = new MessageFormat(pattern,local);
        var format = messageFormat.format(new Object[]{
           "Selly", new Date(), 17600
        });

        System.out.println(format);
    }

    /**
     * choice ini lebih berperan seperti if untuk condition
     */
    @Test
    void choiceFormatTest() {
        var pattern = "-1#negatif | 0#kosong | 1#satu | 1<banyak";
        var choiceFormat = new ChoiceFormat(pattern);

        System.out.println(choiceFormat.format(-1));
        System.out.println(choiceFormat.format(0));
        System.out.println(choiceFormat.format(1));
        System.out.println(choiceFormat.format(2));
    }

    @Test
    void choiceFormatMessageTest() {
        var local = new Locale("in", "ID");
        var resourceBundle = ResourceBundle.getBundle("message", local);

        var pattern = resourceBundle.getString("balance");
        var messageFormat = new MessageFormat(pattern, local);
        System.out.println(messageFormat.format(new Object[]{-500000}));
        System.out.println(messageFormat.format(new Object[]{0}));
        System.out.println(messageFormat.format(new Object[]{400000}));
    }
}
