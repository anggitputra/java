package com.anggit.internationalization;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Dateformat
 * merdukung date ke string dan sebaliknya
 */
public class Date  {

    @Test
    public void dateFormatTest(){
        var pattern = "EEEE dd MMMM yyyy"; //format yang di inginkan
        Locale locale = new Locale("in", "ID");
        var dateFormat = new SimpleDateFormat(pattern,locale);//internationalization set local in simple date format

//        var format = dateFormat.format(new java.util.Date());
//        System.out.println(format); //convert date to string sesuai pattern

        //convert string to date
        try {
            var date = dateFormat.parse("Kamis 04 Juni 2020"); //string string to date
            System.out.println(date);
        }catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
