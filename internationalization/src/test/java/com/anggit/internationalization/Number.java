package com.anggit.internationalization;

import org.junit.jupiter.api.Test;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;

/**
 * Number format jugabisa di internationalization
 * sebagai contoh ketika memformat mata uang
 * bisanya kalo negara inggris dipisahkah dengan coma dan indonesia dipisahkan dengna titik
 */
public class Number {
    @Test
    public void number(){
        var local = new Locale("id","ID");
        var numberFormat = NumberFormat.getInstance(local);
//        var currency = numberFormat.format(55000.80);
//        System.out.println(currency);

        try {
            var result = numberFormat.parse("55.000,80").doubleValue();//disesuaikan dengan indonesia
            System.out.println(result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * ini untuk format currency
     */
    @Test
    public void currency(){
        var local = new Locale("id","ID");
        var currency = Currency.getInstance(local); //untuk mendapatkan data

        System.out.println(currency.getSymbol());
        System.out.println(currency.getDisplayName());
        System.out.println(currency.getCurrencyCode());
    }

    /**
     *berguna untuk memformat currency sesuai negara
     * misal indonesia RP
     */
    @Test
    public void numberFormatCurrency(){
        var local = new Locale("id","ID");
        var currencyLocation = NumberFormat.getCurrencyInstance(local);//menggunakan number format

        var currency = currencyLocation.format(73000.222);
        System.out.println(currency);

        try {
            var currency2 = currencyLocation.parse("Rp600.000,45");
            System.out.println(currency2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
