package com.anggit.internationalization;

import org.junit.jupiter.api.Test;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Internationalization berguna untuk merubah bahasa dengan mudah
 * misal aplikasi digunakan di indonesia maka otomatis nanti menggunakan bahasa indonesia
 *
 * format code language berdasarkan iso 639.2
 * https://www.loc.gov/standards/iso639-2/php/code_list.php
 *
 * Code negara di seluruh dunia
 * https://www.iban.com/country-codes
 */
public class Language {

    @Test
    public void  local(){
        var language = "id";
//        var language = "in";
//        var language = "idn";
        var country = "ID";

        var local = new Locale(language, country); //set language dan country yang digunakan
//        System.out.println("language ="+ local.getLanguage());
//        System.out.println("country ="+ local.getCountry());
//        System.out.println("display language ="+local.getDisplayLanguage());
//        System.out.println("display country ="+local.getDisplayCountry());


        /**
         * saat pembuatan propertise perlu di ingat
         * awal = message (default bahasa jika bahasa di apllikasion propertise tidak ada )
         * indonesia = message_id_ID
         */
        var resourceBundle = ResourceBundle.getBundle("message",local); //set reource bundle berdasalkan propertise dan local language
        System.out.println(resourceBundle.getString("hello"));
        System.out.println(resourceBundle.getString("goodbye"));

    }
}
