package com.anggit.typedataconversionoperator;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Arrays;

public class TypeDataConversionOperator {
    public static void main(String[] args) {
        /*
            Data type in java are divided into two group
            group one(PRIMITIVE) :
                Byte	1 byte	numbers -128 to 127
                Short	2 bytes	numbers -32,768 to 32,767
                Int	    4 bytes	number -2,147,483,648 to 2,147,483,647
                Long	8 bytes	number -9,223,372,036,854,775,808 to 9,223,372,036,854,775,807 (best data type for ID)
                Float	4 bytes	6 to 7 decimal digits
                Double	8 bytes	15 decimal digits
                Boolean	1 bit	true/false
                Char	2 bytes	character(ascii) only

                note : 
                - final di variable menandakan nilainya constan
                - sesuaikan penggunaan type data ini untuk menghemat resource
                - dalam implementasinya type data primitiv jarang di gunakan dan dirubah (int = Integer)
        */
        primitive();

        //group two(NONE PRIMITIVE) : String, Array, Classes and primitiv value with uppercase in fisrt charracter (Integer)
//        nonePrimitive();

        //arithmetic = "+, -, *, /, %
//        arithmetic();

        //assignment = " =, +=, -=, *=, /=, %= "
//        assignment();

        //comparison = ">, <, ==, >=, <=,"
        //note: comparison dengna == hanya cocok untuk primitiv kalo object pake equals
//        comparison();

        //logical = "||, &&, !"
//        logical();

        //note : Bitwise and ternary rarely
    }

    public static void primitive(){
        // note: 
        // widening casting(otomatis)
        // byte b = 10;
        // Int a = b;

        // narrow casting(manual konversi) karena nilainya bisa lebih besar

        //define variable with max value for data type in use
        byte bytes = 127;
        System.out.println("Conversion Byte");
        System.out.println("String: "+ Byte.toString(bytes));
        System.out.println("Integer: "+ Byte.toUnsignedInt(bytes));
        System.out.println("Long: "+ Byte.toUnsignedLong(bytes) );

        short shorts = 32767;
        System.out.println("Conversion short");
        System.out.println("String: "+ Short.toString(shorts));
        System.out.println("Integer: "+ Short.toUnsignedInt(shorts));
        System.out.println("Long: "+ Short.toUnsignedLong(shorts));

        int integer = 2147483647;
        System.out.println("Conversion integer");
        System.out.println("String: "+ Integer.toString(integer));
        System.out.println("String: "+ Integer.toUnsignedString(integer));
        System.out.println("Short: "+ (short) integer); //short -1 because integer greater than short
        System.out.println("Float: "+ (float) integer); //be careful convert int to float
        System.out.println("Double: "+ (double) integer); //be careful convert int to Double
        System.out.println("Long: "+ Integer.toUnsignedLong(integer));
        System.out.println("Char: "+ (char) integer); //no result because char ascii only


        long longs = 9223372036854775807L; //for long add L in end number
        System.out.println("Conversion Long");
        System.out.println("String: "+ Long.toString(longs));
        System.out.println("String: "+ Long.toUnsignedString(longs));
        System.out.println("Short: "+ (short) longs); //short -1 because integer greater than short
        System.out.println("Float: "+ (float) longs); //be careful convert int to float
        System.out.println("Double: "+ (double) longs);  //be careful convert int to Double
        System.out.println("Char: "+ (char) longs); //no result because char ascii only

        float floats = 3.402823E+38f; //340,282,300,000,000,000,000,000,000,000,000,000,000
        System.out.println("Conversion float");
        System.out.println("String: "+ Float.toString(floats));
        System.out.println("String: "+ String.valueOf(floats));
        System.out.println("Double: "+ (double) floats);

        double doubles =  1.7976931348623157E+308;
        System.out.println("Conversion double");
        System.out.println("String: "+ Float.toString(floats));
        System.out.println("String: "+ String.valueOf(floats));
        System.out.println("Double: "+ (double) floats);

        boolean booleans = true;
        System.out.println("Conversion Boolean");
        System.out.println("String: "+ Boolean.toString(booleans));
        System.out.println("String: "+ String.valueOf(booleans));

        char chars = 65; //visit documentation  https://www.asciitable.com/ or ibm.com/docs/en/aix/7.2?topic=adapters-ascii-decimal-hexadecimal-octal-binary-conversion-table
        System.out.println("Conversion Char");
        System.out.println("Char: "+ chars);
        System.out.println("String: "+ Character.toString(chars));

    }

    public static void nonePrimitive(){
        String username = "Jackson 1997"; //support numeric and character
        System.out.println("Username :"+ username);

        // array
        byte[] name = username.getBytes(StandardCharsets.UTF_8);
        System.out.println("Name: "+ name.toString()); //can't read
        System.out.println("Name: "+ String.valueOf(name)); // can't read
        System.out.println("Name: "+ new String(name,StandardCharsets.UTF_8)); // no error because same standard utf 8

        byte[] bytes = new byte[8];
        System.out.println("Conversion to String");
        System.out.println("String bytes: "+ new String(bytes)); //can't read
        System.out.println("String bytes: "+ String.valueOf(bytes)); //can't read
        System.out.println("String bytes: "+ Arrays.toString(bytes)); // full number
        System.out.println("String bytes: "+ new String(bytes, StandardCharsets.UTF_8));

        int[] integers = new int[]{1,2,3,4,5};
        System.out.println("String integers: "+ String.valueOf(integers));  //[I@e9e54c2
        System.out.println("String integers: "+ Arrays.toString(integers)); //[1, 2, 3, 4, 5]

    }

    public static void arithmetic (){
        int a = 5;
        int b = 2;
        int c = 6;
        System.out.println("Basic");
        System.out.println(a + " + " + b + " = "+ (a+b));
        System.out.println(a + " - " + b + " = "+ (a-b));
        System.out.println(a + " * " + b + " = "+ (a*b));

        System.out.println("Modulo");
        System.out.println(a + " % " + b + " = "+ a+b);
        System.out.println(c + " % " + b + " = "+ (c%b));

        System.out.println("For Division");
        System.out.println(a + " / " + b + " = "+ (a/b)); // 5 / 2 = 2 not recommendation int for Division
        System.out.println(c + " / " + b + " = "+ (c/b));

        System.out.println("For number");
        float A = 5.12f;
        float B = 2.3f;
        float resultFloat = A/B;
        System.out.println("Result Float: "+resultFloat); //Result Float: 2.2260869

        System.out.println("Best For Currency Type Data");
        BigDecimal shoesPrice = new BigDecimal(100000.50);
        BigDecimal discount = new BigDecimal(2.5);
        BigDecimal addition = shoesPrice.add(discount);
        System.out.println("BigDecimal Addition (+): "+ addition); //BigDecimal Addition (+): 100003.0
        BigDecimal subtraction = shoesPrice.subtract(discount);
        System.out.println("BigDecimal Subtraction (-): "+ subtraction); //BigDecimal Subtraction (-): 99998.0
        BigDecimal multiplication = shoesPrice.multiply(discount);
        System.out.println("BigDecimal Multiplication (*): "+ multiplication); //BigDecimal Multiplication (*): 250001.25
        BigDecimal division = shoesPrice.divide(discount);
        System.out.println("BigDecimal Division (/): "+ division); //BigDecimal Division (/): 40000.2
    }

    public static void  assignment(){
        int a = 15; //inserting value to variable
        int b = 3;
        a+=b;
        System.out.println("Addition(+): " + a); //Addition(+): 18
        a-=b;
        System.out.println("Subtraction(-): "+a); //Subtraction(-): 15
        a*=b;
        System.out.println("Multiplication(*): "+ a); //Multiplication(*): 45
        a/=b;
        System.out.println("Division(/): "+a); //Division(/): 15
        a%=b;
        System.out.println("Modulo(%): "+a); //Modulo(%): 0
    }

    public static void  comparison(){
        int a = 7;
        int b = 3;
        System.out.println("A > B :"+ (a>b)); //A > B :true
        System.out.println("A < B :"+ (a<b)); //A < B :false
        System.out.println("A == B :"+ (a==b)); //A == B :false
        System.out.println("A >= B :"+ (a>=b)); //A >= B :true
        System.out.println("A <= B :"+ (a<=b)); //A <= B :false

        //note : comparison result boolean type data = (true/false)
    }

    public static void logical(){
        boolean a = true;
        boolean b = false;
        System.out.println(" A && B: "+ (a&&b)); //A && B: false
        System.out.println(" A || B: "+ (a||b)); //A || B: true
        System.out.println(" !A: "+ (!a)); //!A: false
    }

}
