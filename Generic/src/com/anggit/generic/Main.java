package com.anggit.generic;

import com.anggit.generic.entity.Data;
import com.anggit.generic.entity.NumberData;

import java.lang.reflect.GenericArrayType;

/**
 * Java generic sangat unik karena nantinya kita bisa pakai dengan type data yang bisa kita ubah
 * bisa digunakan oleh semua data type
 * generik parameter type yang sering digunakan
 * E = element
 * K = key
 * N = number
 * T = type
 * V = value
 * S.U.V etc
 */
public class Main {
    public static void main(String[] args) {
        genericParam();

        /**
         * generic method
         */
        Integer[] numberList = {1,3,4,5,7};
        System.out.println("lenght Number List: "+ genericMethod(numberList));
        String[] stringList = {"budi","bayu","bagas"};
        System.out.println("Lenght String list:"+ genericMethod(stringList));

        /**
         * Invariant
         * dimana tidak boleh diturunkan
         * Incompatible types. Found: 'com.anggit.generic.entity.Data<java.lang.String>', required: 'com.anggit.generic.entity.Data<java.lang.Object>'
         */
//        Data<String> dataString = new Data<>("Anang");
//        Data<Object> dataObject = dataString;

        /**
         * Covariant
         * berguna agar generic ini bisa di turunkan
         * ditandai gengan ? extend ...
         * Tapi covarian hanya bisa get
         */
        Data<String> dataCovariant = new Data<>("Ageng");
        covariant(dataCovariant);

        /**
         * Contravariant
         * dengan metode ini maka data generic bisa det dan di update
         * tapi hati hati ketika read karena bukan static
         * ditandai dengan ? super String>
         */
        Data<Object> objectData = new Data<>("Intan");
        //Object merupakan superclass dari String
        Data<? super String> stringData = objectData;
        contraVariant(stringData);

        /**
         * Bounded membatasi data type
         * NumberData<T extends Number>
         * untuk multi bounded yang kedua hanya boleh interface
         * karena di java tidak boleh parent lebih dari 1 class
         */
        bounded();

        /**
         * wildcard
         * bertujuan bisa print data yang dikirim tanpa menghiraukan datanya apa
         * ditandai dengan (Data<?> data);
         */
        wildcard(new Data<>("Dian"));
        wildcard(new Data<>(80));

        /**
         * Erasure
         * menghilangkan pengecekan saat runtime jadi ketika develop di ide
         * baik baik saja
         * tapi pada saat compile baru di check sehingga bisa crash
         *
         * Exception in thread "main" java.lang.ClassCastException: class java.lang.String cannot be
         * cast to class java.lang.Integer (java.lang.String and java.lang.Integer are in module java.base of loader 'bootstrap')
         * at com.anggit.generic.Main.main(Main.java:83)
         */
        Data erasureData = new Data<>("Anang");
        Data<Integer> erasureInt = (Data<Integer>) erasureData;
        Integer integerErasure = erasureInt.getData();

    }

    public static void genericParam(){
        Data<String> myData = new Data<>("Gilang");
        System.out.println("Generic Param: "+myData.getData());
    }

    public static <T> Integer genericMethod(T[] array){
        return array.length;
    }

    public static void covariant(Data<? extends Object> data){
        Object object = data.getData();
        System.out.println("Covariant: "+ object);
    }

    public static void contraVariant(Data<? super String> data){
        data.setData("Intan Putri");//set data
        System.out.println("Contravariant: "+ data.getData());
    }

    public static void bounded(){
        NumberData<Integer> intNumber = new NumberData<>(6000);
        System.out.println("Bounded Number: "+ intNumber.getData());
        // error karena class NumberData di define hanya mampu menerima turunan number
//        NumberData<String> stringName = new NumberData<String>("Tu Bagus");
    }

    public static void wildcard(Data<?> data){
        System.out.println("Wildcard: "+data.getData());
    }
}
