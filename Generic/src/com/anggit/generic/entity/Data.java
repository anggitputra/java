package com.anggit.generic.entity;

/**
 * Generic dini singgle parameter
 * @param <T>
 * jika ingin double <T,K> dipisahkan tanda koma
 */
public class Data<T> {
    private T data;

    public Data(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
