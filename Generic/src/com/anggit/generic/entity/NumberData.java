package com.anggit.generic.entity;

/**
 * Number Data ini hanya bisa menampung turunan Dari Number
 * @param <T>
 */
public class NumberData<T extends Number>{
    private T data;

    public NumberData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
