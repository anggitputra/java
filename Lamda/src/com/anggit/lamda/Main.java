package com.anggit.lamda;

import java.util.*;
import java.util.function.*;

/**
 * lamda mengacu pada anonymous function
 * lamda merupakan versi sederhana anonymous class
 * lengkapnya bisa menuju package java.util.function;
 *
 * note :
 * dengan lamda ini semua functi jadi lebih singkat tanpa perlu
 * kita buat code secara manual
 */
public class Main {
    public static void main(String[] args) {
        /**
         * membuat lamda secara manual
         */
//        lamdaInterface();


        /**
         * java sudah support function  bawaan untuk membuat lamda
         * sehingga tidak perlu lagi membuat manual lamda
         */
//        consumer();
//        function();

        /**
         * lamda Collection
         * dimana ada beberapa feture yang memanfaatkan lamda
         */
//        forEach();
//        ifMethod();
//        mapForeach();

        /**
         * Lazy parameter
         * kegunaanya supaya hanya dipanggil ketika diakses
         * ini sangat mempernaruhi kinerja memori supaya lebih cepat
         * dan efisien
         */
        String name = "Agus"; //Selamat Agus berhasil
        String name1 = null; //Maaf null gagal 20\
//        lazyParameter(60, getName());
//        lazyParameter(60, () -> getName()); //di panggil karena nilanya 60
//        lazyParameter(40, () -> getName()); //laziparameter

        /**
         * Optional class di lamda sebagai wrapper valau yang berisi null
         * sehingga bisa menganggunalangi null pointer exception
         */
        optional("Ahmad");
        //optional(null);//Exception in thread "main" java.lang.NullPointerException
//        optional(null);


    }

    public static void optional(String name){
        Optional<String> optName = Optional.ofNullable(name);//check name
        //fullcode
//        Optional<String> optionalNameUpper = optName.map(new Function<String, String>() { //merubah ke upper
//            @Override
//            public String apply(String s) {
//                return s.toUpperCase();
//            }
//        });

        //diubah ke lamda
//        Optional<String> optionalNameUpper = optName.map(s -> s.toUpperCase());

        //diubah ke method reference
        Optional<String> optionalNameUpper = optName.map(String::toUpperCase);

        //full code
//        optionalNameUpper.ifPresent(new Consumer<String>() {//jika ada nilai maka di print
//            @Override
//            public void accept(String s) {
//                System.out.println("Hello"+ s);
//            }
//        });

        //with lamda
        optionalNameUpper.ifPresent(s -> System.out.println("Hello"+ s));

//        String uppername = name.toUpperCase();
//        System.out.println(uppername);
    }


//    public static void lazyParameter(Integer value, String name){
    public static void lazyParameter(Integer value, Supplier<String> name){
//        if (value>50) System.out.println("Selamat "+ name +" berhasil");
        if (value>50) System.out.println("Selamat "+ name.get() +" berhasil");//get value dari lazyparameter
//        else System.out.println("Maaf " + name +" gagal");
        else System.out.println("Maaf gagal");
    }

    public static String getName(){
        System.out.println("getName dipanggin");
        return "Suharsono";
    }

    public static void mapForeach(){
        Map<String,String> map = new HashMap<>();
        map.putAll(Map.of(
                "one","Budi",
                "two","Bela"));

        //manual code with biconsumer
        map.forEach(new BiConsumer<String, String>() {
            @Override
            public void accept(String s, String s2) {
                System.out.println(s + " value "+ s2);
            }
        });

        // dengan lamda
        map.forEach((s, s2) -> System.out.println(s + " value "+ s2));
    }

    public static void ifMethod(){
        List<String> names = new ArrayList<>();
        names.addAll(List.of("Erwin","erwan","evan","Emanuel"));

//        dengan predicate
        names.removeIf(new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.length() > 5;
            }
        });

        //dengan lamda lebih singkat
        names.removeIf(s -> s.length() > 5);

        System.out.println(names);
    }

    public static void forEach(){
        List<String> names = List.of("Eva","Evi");
        //manual fullcode
        names.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });

        //dengan lamda
        names.forEach(s -> System.out.println(s));

        //dengan methode reference
        names.forEach(System.out::println);
    }

    /**
     * Function <T,R>
     *     T sebagai type data parameter
     *     R sebagai return
     */
    public static void function(){
        //full code
        Function<String, Integer> function = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        };
        System.out.println("Function length ="+ function.apply("Gilang Dirga"));

        //dengan lamda
        Function<String, Integer> function1 = s -> s.length();
        System.out.println("Function length1 ="+ function1.apply("Galang Ramadhan"));

        //dengan method reference
        Function<String, Integer> function3 = String::length;
        System.out.println("Function length3 ="+ function3.apply("Gumelar"));

    }

    public static void consumer(){
        //Interface Consummer
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };
        consumer.accept("Budi");

        //replace dengan lamda
        Consumer<String> consumer1 = s -> System.out.println(s);
        consumer1.accept("Eva");

        //dengan method reference yang merupakan paling singgkat
        Consumer<String> consumer3 = System.out::println;
        consumer3.accept("Furqon");

    }

    public static void lamdaInterface(){
        //manual
        SimpleAction simpleAction1 = new SimpleAction() {
            @Override
            public String action(String name) {
                return "Eka";
            }
        };
        System.out.println("simpleAction1 :"+ simpleAction1);

        //dengan lamda reference (lamda tanpa block)
        SimpleAction simpleAction2 = name -> "Anggit";
        System.out.println("simpleAction2 :"+ simpleAction2);

        //ini juga sam dengan simpleaction 2
        SimpleAction simpleAction3 = (name) -> "Anggit";
        System.out.println("simpleAction2 :"+ simpleAction2);
    }
}
