package com.anggit.lamda;

/**
 * menandakan lamda function dengan syarat
 * - memiliki 1 method abstract
 * - berupa interface
 */
@FunctionalInterface
public interface SimpleAction {
    String action(String name);
}
