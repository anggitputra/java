package com.anggit.junit.repository;

import com.anggit.junit.data.Person;

public interface PersonRepository {
    Person selectById(String id);
    void insertPerson(Person person);
}
