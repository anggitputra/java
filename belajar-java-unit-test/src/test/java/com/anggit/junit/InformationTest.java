package com.anggit.junit;

import org.junit.jupiter.api.*;

/**
 * TestInfo info
 * ini semua untuk injection agar dapat mengetahui value dari tag displayname dan lainya
 */
@DisplayName("Information Test")
public class InformationTest {

    @Test
    @DisplayName("testing")
    @Tags({
            @Tag("test1"),
            @Tag("test2")
    })
    public void testing(TestInfo info){
        System.out.println(info.getDisplayName());
        System.out.println(info.getTestClass());
        System.out.println(info.getTags());
    }
}
