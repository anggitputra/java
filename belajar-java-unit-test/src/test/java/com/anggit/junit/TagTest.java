package com.anggit.junit;


import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;

/**
 * Tag sangat berguna untuk kita spesifik mau test di bagian mana
 * misal disini integration-test tinggal di panggil
 *
 * mvn test = untuk running semua test
 * mvn test -Dgroups=integration-test = hanya akan run yang berdasakan tag saja
 *
 * bahkan untuk test ini bisa di otomisasi dnegan menambahkan di bagian configuration running
 * add Junit
 * name : disesuaikan dengan nama yang mudah di ingat
 *
 * -> pilih bagian Tags
 * -> isi nama tags sesuai dengan tags yang ingin di test
 *
 * bagian modify
 *     -> setting sesuai dengan coverage test
 *         whole project = semua dengan tags yang dimaksud
 *         singgle project = hanya di test di bagia yang module di tentukan
 */
@Tags({
        @Tag("integration-test")
})
public class TagTest {
    @Test
    public void test(){ }

    @Test
    public void test1(){}
}
