package com.anggit.junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.*;

import java.util.Properties;

public class ConditionTest {

    @Test
    @EnabledOnOs({OS.LINUX})//check os yang di pakai jika linux maka di executio
    public void runOnlyOnLinux(){ }

    @Test
    @DisabledOnOs({OS.LINUX})
    public void disableOnLinux(){ }

    @Test
    @EnabledOnJre({JRE.JAVA_11})//check java yang di pakai dan bisa multiple
//    @DisabledOnJre({JRE.JAVA_11})
    public void runningOnJava11(){ }

    @Test
    @EnabledForJreRange(min = JRE.JAVA_8, max = JRE.JAVA_11) //dapat running berdasarkan range java runtime yang di set
//    @DisabledForJreRange(min = JRE.JAVA_8, max = JRE.JAVA_11)
    public void runningOnJavaRange(){}

    /**
     * untuk mendapatkan properties  yang ada di komputer
     */
    @Test
    public void getSystemProperties(){
        Properties properties = System.getProperties();
        properties.forEach((key,value) -> System.out.println(key+":"+value));
    }

    /**
     * By system properties
     * untuk sebagai pembanding running test
     */
    @Test
    @EnabledIfSystemProperties({
            @EnabledIfSystemProperty(named = "user.name", matches = "anggit"),
            @EnabledIfSystemProperty(named = "java.version", matches = "11.0.11")
    })
//    @DisabledIfSystemProperties({
//            @DisabledIfSystemProperty(named = "user.name", matches = "anggit"),
//            @DisabledIfSystemProperty(named = "java.version", matches = "11.0.11"),
//    })
    public void enablePropertiesUserName(){}


    /**
     * Hasilnya disable karena di profile belum di set
     * dan ini menggunakan if sebagai condition
     */
    @Test
    @EnabledIfEnvironmentVariables({
            @EnabledIfEnvironmentVariable(named = "PROFILE", matches = "Dev")
    })
//    @DisabledIfEnvironmentVariables({
//            @DisabledIfEnvironmentVariable(named = "PROFILE",matches = "Dev")
//    })
    public void enableOnProfileDev(){}

}
