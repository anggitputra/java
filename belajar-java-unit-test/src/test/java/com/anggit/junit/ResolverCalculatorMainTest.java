package com.anggit.junit;

import com.anggit.junit.resolver.RandomParameterResolver;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extensions;

@Extensions({
        @ExtendWith(RandomParameterResolver.class)
})
public class ResolverCalculatorMainTest {
    protected Calculator calculator = new Calculator();
}
