package com.anggit.junit;

import org.junit.jupiter.api.*;

import java.util.LinkedList;
import java.util.Queue;

@DisplayName("A Queue")
public class TestInTest {

    private Queue<String> queue;

    /**
     * nested disini berguna dimana menandakan whenNew merupakan inner class
     * dari testintest
     *
     * tujuan dibuat ini agar test berada dalam 1 covverage file class testintest
     */
    @Nested
    @DisplayName("When New")
    public class whenNew{
        @BeforeEach
        public void setUp(){
            queue = new LinkedList<>();
        }

        @Test
        @DisplayName("When Offer Data, Size must be 1")
        public void offerNewData(){
            queue.offer("Anggit");
            Assertions.assertEquals(1,queue.size());
        }

        @Test
        @DisplayName("When offer 2 data, size must be 2")
        public void offerMoreData(){
            queue.offer("Anggit");
            queue.offer("Putra");
            Assertions.assertEquals(2, queue.size());
        }
    }
}
