package com.anggit.junit;

import org.junit.jupiter.api.*;

//sebagai tanda untuk mengurutkan
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LifeCycleTest {

    private int counterNumber = 0;

    @Test
    @Order(2)
    public void testA(){
        counterNumber++;
        System.out.println("TestA =>"+counterNumber);
    }

    @Test
    @Order(1)
    public void testB(){
        counterNumber++;
        System.out.println("TestB =>"+counterNumber);
    }
}
