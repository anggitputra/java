package com.anggit.junit.service;

import com.anggit.junit.data.Person;
import com.anggit.junit.repository.PersonRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.Extensions;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


/**
 * Extensions dengan menggunakan ini bisa menggunakan berbagai @ExtendWith
 */
@Extensions({
        @ExtendWith(MockitoExtension.class)
})
public class PersonServiceTest {

    /**
     * menandakan person repository dibuat mocking
     */
    @Mock
    private PersonRepository personRepository;
    private PersonService personService;

    /**
     * sebelum di test person servive di setup connect ke person repository
     */
    @BeforeEach
    public void setUp(){
        personService = new PersonService(personRepository);
    }

    @Test
    public void personNotFound(){
        Assertions.assertThrows(IllegalArgumentException.class, () ->{
            personService.get("Not Found");
        });
    }

    /**
     * mockito dibuat data jika selectid one makan akan di return one beserta namanya Anggit
     */
    @Test
    public void personAvailable(){
        Mockito.when(personRepository.selectById("one"))
                .thenReturn(new Person("one","Anggit"));

        var person = personService.get("one");
        Assertions.assertNotNull(person);
        Assertions.assertEquals("Anggit",person.getName());
    }

    /**
     * pada register person ini dibuat verifikasi bahwa personRepository harus di di bagian insertperson 1 kalo
     */
    @Test
    void registerPerson(){
        var person = personService.register("Anggit");
        Assertions.assertNotNull(person);
        Assertions.assertEquals("Anggit", person.getName());
        Mockito.verify(personRepository, Mockito.times(1))
                .insertPerson(new Person(person.getId(), "Anggit"));
    }

}
