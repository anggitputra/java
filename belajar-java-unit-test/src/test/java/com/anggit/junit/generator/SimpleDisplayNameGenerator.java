package com.anggit.junit.generator;

import java.lang.reflect.Method;

/**
 * Displaynamegenerator untuk kita seting sendiri sehingga bisa menyusuaikan yang harus di tampilkan
 * tujunaya untuk mempermudah penamaan testing
 */
public class SimpleDisplayNameGenerator implements org.junit.jupiter.api.DisplayNameGenerator {
    @Override
    public String generateDisplayNameForClass(Class<?> testClass) {
        return "Test "+ testClass.getSimpleName();
    }

    @Override
    public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
        return null;
    }

    @Override
    public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
        return "Test "+ testClass.getSimpleName()+ "."+ testMethod.getName();
    }
}
