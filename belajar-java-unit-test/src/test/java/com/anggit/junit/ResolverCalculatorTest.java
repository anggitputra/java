package com.anggit.junit;

import com.anggit.junit.resolver.RandomParameterResolver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Random;


public class ResolverCalculatorTest extends ResolverCalculatorMainTest {
    /**
     * random disini merupakan parameter resolver yang di deklarasikan secara manual
     * @param random
     */
    @Test
    public void testRandom(Random random){
        Integer a = random.nextInt();
        Integer b = random.nextInt();
        Integer result =  calculator.add(a,b);
        Integer expected = a + b;

        Assertions.assertEquals(expected, result);
    }
}
