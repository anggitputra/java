package com.anggit.junit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

import java.util.concurrent.TimeUnit;

/**
 * dengan penambahan @Execution(ExecutionMode.CONCURRENT)
 * maka test akan berjalan bersamaan secara councurrency dan tidak saling menunggu
 *
 * ada yang perlu di persiappkan
 * di bagian resource untuk setting on
 * junit.jupiter.execution.parallel.enabled = true
 */
@Execution(ExecutionMode.CONCURRENT)
public class TimeoutTest {

    /**
     *Timeout berguna untuk menentukan berapa lama menunggu response
     * @throws InterruptedException
     *
     * result erro java.util.concurrent.TimeoutException: slowTest() timed out after 2 seconds
     */
    @Test
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    public void slowTest() throws InterruptedException{
        Thread.sleep(10000);
    }

    @Test
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    public void slowTest1() throws InterruptedException{
        Thread.sleep(10000);
    }

    @Test
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    public void slowTest2() throws InterruptedException{
        Thread.sleep(10000);
    }

    /**
     * code program diatas untuk menunjukkan Test berjalan secara antrian
     * tidak councurent dan selesai dalam 6,75 detik
     */
}
