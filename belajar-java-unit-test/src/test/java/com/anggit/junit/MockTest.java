package com.anggit.junit;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;


public class MockTest {

    /**
     * manual membuat mocking
     */
    @Test
    public void simpleMock(){
        //membuat mock object dengan mockito
        List<String> list= Mockito.mock(List.class);

        //memasukkan nilai kedalam list
        Mockito.when(list.get(0)).thenReturn("Anggit");
        System.out.println(list.get(0));
//        System.out.println(list.get(1));
        //memvalidasi bahwa mockito bagian get list index ke 0 hanya boleh di panggil 1 kali
        Mockito.verify(list, Mockito.times(1)).get(0);
    }

}
