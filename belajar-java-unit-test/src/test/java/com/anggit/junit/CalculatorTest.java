package com.anggit.junit;

import com.anggit.junit.generator.SimpleDisplayNameGenerator;
import org.junit.jupiter.api.*;
import org.opentest4j.TestAbortedException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

/**
 * run all unit test in class calculattor
 */
//@DisplayName("Test untuk calculator class") //manual set name
@DisplayNameGeneration(SimpleDisplayNameGenerator.class) //autho generate dengan sesuia yang di inginkan
public class CalculatorTest {

    //import calculator
    private Calculator calculator = new Calculator();

    /**
     * Hanya di static
     * Hanya akan di jalankan ketika semua unit test sebelum di jalankan
     */
    @BeforeAll
    public static void beforeAll(){
        System.out.println("Before All test");
    }

    /**
     * Hanya di static
     * Hanya akan di jalankan ketika semua unit test di jalankan selesai
     */
    @AfterAll
    public static void afterAll(){
        System.out.println("After All test");
    }

    /**
     * bertujuan untuk di tambahkan sebelum unit test di jalankan
     */
    @BeforeEach
    public void setUp(){
        System.out.println("Before Unit test");
    }

    /**
     * bertujuan untuk di tambahkan setelah unit test di jalankan di semua method
     */
    @AfterEach
    public void  tearDown(){
        System.out.println("After Unit test running");
    }

    /**
     * running add success
     * Displayname untuk menampilkan nama di hasil execution
     */
    @Test
//    @DisplayName("Test skenario untuk add success(Integer,Integer)")
    public void  addSuccessTest(){
        var res = calculator.add(5,5);

        //manual test dan tidak di rekomendasikan
//        if (res != 10)throw new RuntimeException("Test Gagal");

        //menggunakan assertion lebih jelas data errornya
        assertEquals(10,res);
    }

    @Test
    public void devideTest(){
        var res = calculator.devide(50,5);
        assertEquals(10,res);
    }

    @Test
    public void devideTestFailedTest(){
        //IllegalArgumentException disesuiakan dengan class yang di test
        assertThrows(IllegalArgumentException.class, () -> {
            calculator.devide(10,0);
        });
    }

    @Test
    @Disabled //untuk disable unit test
    public void testComingSoon(){

    }

    /**
     * Env bukan samadengan dev maka bisa di batalkan
     */
    @Test
    public void abortedTest(){
        var profile = System.getenv("PROFILE");
        if (!"DEV".equals(profile)){
            throw new TestAbortedException("Test Dibatalkan karena bukan env dev");
        }
    }

    /**
     * Hanya akan di jalankan ketika sama
     * ini merupakan versi simple dari test abortedTest
     */
    @Test
    public void assumptionTest(){
        assumeTrue("DEV".equals(System.getenv("PROFILE")));
    }

}
