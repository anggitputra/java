J Unit:
Merupakan test framework yang paling populer digunakan di java

Test dalam Development:
- Ui        = biaya mahal karena akan tes sekacara keseluruhan
- Service   = test sedikit makal karena berkain dengan bagia yang menjadi satu (prose stransaction)
- Unit      = test lebih kecil yaitu test terhadap method

method test:
    method test di tandai dengna @Test
    pembuatan method sesuai nama method yang di test ditambahkan test

unit tes:
    - memiliki sifat independen antar method sehingga jangan ambil value di method lain yang sama sama test
    - sehingga tidak urut saat di execution
    - jika ingin menjadi urut harus di set terlebih dahulu
        contoh :
        @TestMethodOrder(MethodOrderer.MethodName.class) = diurutkan berdasarkan penamaan dan ini di taruh diatas class
        @TestMethodOrder(MethodOrderer.MethodName.class) = maka akan di random walaupun tampilannya urut
        @TestMethodOrder(MethodOrderer.OrderAnnotation.class)  = akan di urutkan berdasarkan set kita
            contoh :
                @Test
                @Order(2)
                public void testA(){}
    - supaya beberapa method bisa mengakses value yang sama dengan menggunkaan 
        contoh :
        tambahkan diatas class
        @TestInstance(TestInstance.Lifecycle.PER_CLASS) //yang menjadi default permethod(tetapi kalo permethod,method yang lain tidak bisa saling akses value)


 