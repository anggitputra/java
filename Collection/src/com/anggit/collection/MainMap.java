package com.anggit.collection;


import java.util.*;

/**
 * Map interface
 * map memiliki key dan value
 * perbedaan dengan array dimana map ini key ditentukan oleh kita
 */
public class MainMap {
    public static void main(String[] args) {

        /**
         * Hashmap
         * hashmap menggunakan distribusi hashCode
         * menggunakan hashmap bisa semakin lambat jika  hascode banyak kemirikpan
         * penggecekan kemiripan menggunakan equal
         */
//        hashMap();

        /**
         * WeakHashMap
         * memiliki keunikan dimana jika datanya sudah lama tidak digunakan akan bisa terhapus
         * jika terjadi pemanggilan terhadap garbage collection
         * cocok digunakan untuk menyimpan data cache
         */
//        weakHashMap();

        /**
         * IndentityHashMap
         * pencocokan data menggunakan == dan bukan equal
         */
//        identityHashMap();

        /**
         * LinkedHashMap
         * implementasi map dengan double linked list
         * penyimpanan berdasarkan urutan
         */
//        linkedHashMap();

        /**
         * EnumMap
         * dimana key menggunakan enum
         * ini lebih optimal di banding dengan hashcode karena
         * enum sudah pasti unik
         */
//        enumMap();

        /**
         * Immutable map dimana datanya tidak bisa dirubah
         * atau yang lainya
         */
//        immutableMap();

        /**
         * SortedMap
         * cirikhasnya daa key diurutkan sesuai dengan comparable key / comparator
         * ini cocok untuk kasus yang keynya harus berurut
         */
//        sortedMap();

        /**
         * Navigable Map
         * ini merupakan turunan dari sortedmap
         * memiliki operasi kurang dan lebih dari dan dll
         */
//        navigableMap();

        /**
         * Entry Map
         * entry merupakan interface sederhana untuk mengambil key dan value
         */
        entryMap();
    }

    public static void entryMap(){
        Map<String,String> names = new HashMap<>();
        names.putAll(Map.of(
                "one","Susi",
                "two","Suhestuti",
                "three","Susilo"
        ));

        Set<Map.Entry<String,String>> entries = names.entrySet();
        System.out.println("Testing EntryMap");
        for (var entry:entries){
            System.out.println("Key : "+ entry.getKey());
            System.out.println("Value : "+ entry.getValue());
        }
    }

    public static void navigableMap(){
        NavigableMap<String,String> names = new TreeMap<>();
        names.put("one","Resa");
        names.put("two","Reza");
        names.put("three","Rosi");

        System.out.println("Navigation map list:");
        System.out.println(names.higherKey("three"));
        System.out.println(names.higherEntry("Reza"));
        //note:berdasarkan urutan penyimpanan
    }

    public static void sortedMap(){
        SortedMap<String , String> names = new TreeMap<>();
        names.put("one","Pian");
        names.put("two","Putu");
        names.put("three","Putri");

        System.out.println("List with sorted map:");
        for (var key : names.entrySet()){
            System.out.println(key);
        }

        SortedMap<String , String> immutableNames = Collections.emptySortedMap();
        System.out.println("immutableNames = "+ immutableNames);
    }

    public static void immutableMap(){
         Map<String ,String > names = Map.of(
                 "one","Opri",
                 "two","Omesh",
                 "three", "Oneng"
         );

//        at com.anggit.collection.MainMap.immutableMap(MainMap.java:xx)
//         names.put("four","Opa");
    }

    //Lever merupakan anonymous class
    public static enum Level{
        Satu,Dua,Tiga,Empat
    }

    public static void enumMap(){
        EnumMap<Level, String> names = new EnumMap<>(Level.class);
        names.put(Level.Satu, "Novita");
        names.put(Level.Dua, "Nanda");
        names.put(Level.Tiga, "Nastitong");
        names.put(Level.Empat, "Novi");

        System.out.println("EnumMap");
        System.out.println("Names level 3 = "+ names.get(Level.Tiga));
    }

    public static void linkedHashMap(){
        Map<String , String > names = new LinkedHashMap<>();
        names.put("one","Mega");
        names.put("two","Muhammad");
        names.put("three","Maulana");

        System.out.println("List with linkedHashmap :");
        for (var key : names.entrySet()) {
            System.out.println(key);
        }
    }

    public static void identityHashMap(){
        Map<String,String> names = new IdentityHashMap<>();
        names.put("one","Lintang");
        names.put("two","Listia");
        names.put("three","Lidya");

        System.out.println("Map size with identityHashMap");
        System.out.println(names.size());
    }

    public static void weakHashMap(){
        Map<Integer,String> listNumber = new WeakHashMap<>();

        for (int i = 0; i<150; i++){
            listNumber.put(i,"Number"+String.valueOf(i));
        }
        //call garbage collector
        System.gc();

        System.out.println("size data WeakHashMap");
        System.out.println(listNumber.size());
    }

    public static void hashMap(){
        Map<String,String> names = new HashMap<>();
        names.put("one","Kusuma");
        names.put("two","Kelvin");
        names.put("three","Kevin");
        names.put("four","Kurnia");
        System.out.println("Show List name with hashmap: ");
        System.out.println(names.get("three"));

        System.out.println(names);

    }
}
