package com.anggit.collection;

import com.anggit.collection.data.Person;
import com.anggit.collection.data.PersonComparator;

import javax.imageio.plugins.jpeg.JPEGImageReadParam;
import javax.xml.validation.Validator;
import java.util.*;

/**
 * Collection di java dibagi menjadi 2
 * - iterrable = yang berupa element
 * - map = yang berupa key and value
 * semua collection mendukung perulangan for each
 */
public class MainIterable {
    public static void main(String[] args) {
        //parent class collection
//        iterable();

        /**
         *merupakan child dari iterable dan merupakan parent dari yang lain
         * kehebatan collection ini bisa edit delete dan isert dan hapus
         */
//        collection();

        /**
         * List = arraylist || linklist
         * arraylist = memiliki default 10 array tetapi bisa di set juga
         * linklist = tidak memiliki defaul tepai agak lambat ketika get set
         */
//        list();
        /**
         * Set = (enumSet, hashset, linkedHashset)
         * Set disini juga menampung data berupa array
         * tapi yang unik dimana kalo set tidak boleh ada data yang smaa
         * hashset = data tidak berurut
         * linkedset = data diurutkan berdasarkan waktu dibuat
         */
//        set();

        /**
         * SortedSet masih turunan dari set
         * untuk mengurutkan bisa menggunaka comparator
         */
//        sortedSet();

        /**
         * Navigable
         * berguna untuk pencarian element yang lebih besar atau sebaliknya
         */
//        navigable();

        /**
         * queue merupakan antrian first in first out
         * array deque   = menggunakan array sebagai implementasinya
         * linkedlist    = menggunakan double linked untuk implementasinya
         * priorityqueue = menggunakan array tapi pengurutan menggunakan comparator
         */
//        queue();
//        priorityQueue();

        /**
         *Dequeue
         * metode pengurutan menggunaka Last in First Out
         * support
         * arrayDeque
         * LinkedList
         */
        dequeue();
    }

    public static void dequeue(){
        Deque<String> names = new ArrayDeque<>();
        names.offerFirst("Johan");
        names.offerLast("Jejes");
        names.offerLast("Janti");
        System.out.println("List with dequeue");
        for (var name = names.pollLast(); name != null; name = names.pollLast()){
            System.out.println(name);
        }
    }

    public static void priorityQueue(){
        Queue<String> name = new PriorityQueue<>();
        name.addAll(Set.of("Ilham","Imam","Intan","Indah"));

        System.out.println("List with Priority Queue :");
        for (var next = name.poll(); next!= null; next=name.poll()){
            System.out.println(next);
        }
        //name juga menjadi size 0 jika udah di tampilkan
        System.out.println(name.size());
    }

    public static void queue(){
        Queue<String> name = new ArrayDeque<>(10);
        name.offer("Hari");
        name.addAll(Set.of("Hengky","Hilda","Huda"));

        System.out.println("List queue name :");
        for (String next = name.poll(); next != null; next = name.poll()){
            System.out.println(next);
        }

        //size o karena semua udah di tampilkan
        System.out.println(name.size());
    }

    public static void navigable(){
        NavigableSet<String> names = new TreeSet<>();
        names.addAll(Set.of("Gilang","Galuh","Galeh", "Gendut"));

        NavigableSet<String> nameDesc = names.descendingSet();
        System.out.println("Navigable List Name :");
        for (var name : names){
            System.out.println(name);
        }

        //maka navigable ini dibuat tidak ada nilaynya
//        NavigableSet<String> navigable = Collections.emptyNavigableSet();
    }

    public static void sortedSet(){
        //huruf besar kecil bepengaruh
        SortedSet<Person> people = new TreeSet<>(new PersonComparator());
        people.add(new Person("Firdaus"));
        people.add(new Person("Farhan"));
        people.add(new Person("Fitri"));
        people.add(new Person("fahri"));

        System.out.println("SortedSet List Name :");
        for (var person : people){
            System.out.println(person.getName());
        }

        //otomatis tidak bisa di tambahkan karena di set kosong
        SortedSet<Person> imuttable = Collections.unmodifiableSortedSet(people);
    }

    public static void set(){
        Set<String> employee = new HashSet<>();
//        Set<String> employee = new LinkedHashSet<>();
        employee.addAll(Arrays.asList("Evan","Erwin","Evi","Eko"));

        System.out.println("hashSet And LinkedSet mutable");
        for (var name : employee){
            System.out.println(name);
        }
//        Set<String> employeeUnmodify = Collections.unmodifiableSet(employee); //membuat tidak bisa di modif

        System.out.println("hashset And Linkedset immutable");
        Set<String> country = Collections.emptySet();
        country = Collections.singleton("Indonesia");//add dengan spesifik
        for (var name : country){
            System.out.println(name);
        }
    }

    public static void list(){
        //new Arraylist menandakan muttable yang
        // artinya bisa di tambah, diubah dan delete
        List<String> employee = new ArrayList<>();
//        List<String> employee = new LinkedList<>();
        employee.addAll(Arrays.asList("Dude","Dedi","deni"));

        System.out.println("List Name With List Mutable");
        for (var name : employee){
            System.out.println(name);
        }

        //konversion ke immutable list
        employee = Collections.unmodifiableList(employee);
        System.out.println("List Name With List Immutable");
        for (var name : employee){
            System.out.println(name);
        }
        //at java.base/java.util.Collections$UnmodifiableCollection.add(Collections.java:1060)
//        employee.add("Dama");

//        employee = Collections.emptyList(); //untuk membuat empty
//        employee = Collections.singletonList("Dian");// untuk menambah 1 singgle

    }

    public static void collection(){
        Collection<String> employee = new ArrayList<>();
        employee.add("Cinta");
        employee.addAll(Arrays.asList("Cici", "Caca", "Chichi"));
        employee.remove("Cici");
        System.out.println("List Name With Collection:");
        for (var name : employee){
            System.out.println(name);
        }
    }

    public static void iterable(){
        Iterable<String> employe = List.of("Dian","Danti","Danik");
        System.out.println("List Name With Itterable");
        //perulangan ini bisa terjadi karena iterator
        for (var name : employe){
            System.out.println(name);
        }
    }
}
