package com.anggit.collection;

import com.anggit.collection.data.SingleQueue;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class MainAnotherCollection {
    public static void main(String[] args) {
        /**
         * legagy collection bagian ini sudah jarang sekali di pakai karena merupakan versi jaman dulu
         * - vector class = mirip dengan array list tapi bersifat syncrhronized
         * - hashtable = mirip dengan map bersifat syncrhonized
         * - stact class = seperti deques
         */

        /**
         * Sorting berguna untuk mengurutkan data
         * tapi hanya bisa berjalanya jika kita menggunakan list
         *
         * Binary search berguna untuk percarian
         * hanya bisa dilakukan jika data telah urut
         */
//        sortingAndBinarySearch();

        /**
         * Collection Class
         * berisikan banyak utility yang sangan membantu
         * void copu, max, min, reverse, shuffle, swap
         */
//        collectionClass();

        /**
         * abstrac collection sangat berguna ketika di implementasi di clas
         * sehingga tidak mengambil semua method yang ada
         * AbstractCollection = Collection
         * AbstractList = List
         * AbstractMap = Map
         * AbstractQueue = Queue
         * AbstractSet = Set
         */
//        abstractCollection();
        /**
         * Default method
         * bertujuan untuk menambahkan konkrit method di interface
         * Default method collection = iterable foreach, list removeif, list replaceall
         * Default method map = getOrderfault, foreach, replace, putifabsensi, remove
         */
//        defaultMethod();
        /**
         * Splitter ini bertujuan untuk partisi data
         * contoh ketika kita memproses ribuan data
         * dengan splitter ini bisa di bagi menjadi beberapa bagia
         */
//        splitter();

        /**
         * Konversi Array
         * Collection memiliki method to array
         * Object[] to array()
         * T[] to array(T[])        = String[] dll
         */
        konversiArray();
    }

    public static void konversiArray(){
        List<String> names = List.of("Eva","Evi","Eni","Erna","Evan");
        Object[] objects = names.toArray();
        String[] strings = names.toArray(new String[]{});

        System.out.println("Konversi Array");
        System.out.println("Objects :"+ Arrays.toString(objects));
        System.out.println("Array :"+Arrays.toString(strings));
    }

    public static void splitter(){
        List<String> names = List.of("Anggit","Anwar","Aziz","Azi",
                "Ahmad","Andri");
        Spliterator<String> split1 = names.spliterator(); //berguna split data awal akan dibagi setengah
        Spliterator<String> split2 = split1.trySplit(); //akan membagi split 1 menjadi 2

        System.out.println(" Splitter data Size :");
        System.out.println("Ret Split1 size ="+ split1.estimateSize());
        System.out.println("ret Split2 size ="+ split2.estimateSize());

    }

    public static void defaultMethod(){
        List<Integer> collection = new ArrayList<>();
        collection.addAll(List.of(1,2,3,4,5,6,7,8,9));

        System.out.println("Collection List :");
        collection.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });

        System.out.println("Map Default Method");
        Map<Integer,String> names = new HashMap<>();
        names.putAll(Map.of(
                1,"Desi",
                2,"Evi",
                3,"Fatma"
        ));

        names.forEach(new BiConsumer<Integer, String>() {
            @Override
            public void accept(Integer integer, String s) {
                System.out.println(integer+" : "+ s);
            }
        });
    }

    public static void abstractCollection(){
        //untuk singgel value
        Queue<String> queue = new SingleQueue<>();
        queue.offer("Budi"); //offer di set hanya untuk 1 value
        queue.offer("Anggit");
        queue.offer("Citra");

        System.out.println("Abstract Collection");
        System.out.println("First Size :"+queue.size());
        System.out.println("Result Peek :"+ queue.peek());
        System.out.println(queue.poll());
        System.out.println("Second Size :"+queue.size());

    }

    public static void collectionClass(){
        List<Integer> listNumber = new ArrayList<>();
        listNumber.addAll(List.of(1,3,6,4,2,7,8,9));

        System.out.println("Collection Class");
        Collections.reverse(listNumber);
        System.out.println("List Reverse :"+ listNumber);
    }

    public static void sortingAndBinarySearch(){
        List<Integer> listNumber = new ArrayList<>();
        listNumber.addAll(List.of(1,3,6,4,2,7,8,9));

        System.out.println("Sorting list :");
        System.out.println("Number List: "+listNumber);

        Collections.sort(listNumber);
        System.out.println("Result Sorting: "+listNumber);

        int index = Collections.binarySearch(listNumber, 7);
        System.out.println("Result Binary idex 7 :"+ index);

//        result
//        Number List: [1, 3, 6, 4, 2, 7, 8, 9]
//        Result Sorting: [1, 2, 3, 4, 6, 7, 8, 9]
//        Result Binary idex 7 :5
    }

}
