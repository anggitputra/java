package com.anggit.collection.data;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {

    @Override
    public int compare(Person person, Person person1) {
        return person.getName().compareTo(person1.getName());
    }
}
