package com.anggit.stream;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.*;

/**
 * Stream Pipeline adalah hal yang paling sering digunakan
 * didunia pekerjaan karena operationnya langsung dan lebil simple
 */
public class StreamOperationTest {


    // lebih clean dan sangat cepat tanpa perlu banyak pembuatan variable
    @Test
    public void pipeline() {
        List<String> employee = List.of("Anggit", "Sisco", "Kelvin");
        employee.stream()
                .map(name -> name.toUpperCase()) //intermediet yang merupakan return void sehingga tidak bisa di print
                .map(name -> "Mr "+name)
                .forEach(System.out::println); //terminal operation = yang berguna untuk menampilkan data
    }

    /**
     * transformation operator
     * berguna untuk merubah bentuk data di data stream
     * saat ini yang didukung adalah MAP dan FLATMAP
     */
    @Test
    public void mapStream(){
        List.of("Anggit","Sisco","Sanoval").stream()
                .map(name -> name.toUpperCase())//map untuk operation yang simple
                .map(name -> name.length()) //perubahan menjadi panjang nama
                .forEach(System.out::println);
    }

    @Test
    public void flatMapStream(){
        List.of("Anggit","Sisco","Sanoval").stream() //untuk perubahan yang advance
                .flatMap(name -> Stream.of(name.length() + ". " + name)) //merubah nilai tetaapi harus di buat stream lagi
                .forEach(System.out::println);
    }

    /**
     * filetering operation
     * beguna unuk melakukan filter terhadap data stream
     * yang tersedia saat ini adalah Distinct dan filter
     */
    @Test
    public void filterStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .filter(number -> number > 5)//berguna untuk memfilter data sesuai keinginan
                .forEach(System.out::println);
    }

    @Test
    public void distinctStream(){
        List.of(1,2,1,3,4,5,5,6,7,8).stream()
                .distinct()//distinct berguna untuk menghapus data yang duplikat
                .forEach(System.out::println);
    }

    /**
     * Retrieving operation
     * ini seperti filter tapi sangat berbeda karena disini bertujuan untuk pengambilan jumalah
     * yang telah di tentukan
     */
    @Test
    public void limitStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .limit(4) //hanya akan mengambil 4 number dari depan
                .forEach(System.out::println);
    }

    @Test
    public void skipStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .skip(4)//akan skip 4 number dari depan kemudian di tampilkan
                .forEach(System.out::println);
    }

    @Test
    public void whileStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .takeWhile(number -> number<4) //yang ditampilkan adanya data yang nilainya kurang dari 4
                .forEach(System.out::println);
    }

    @Test
    public void dropWhileStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .dropWhile(number -> number<4)//yang kurang dari 4 dihapus dan menampilakan yang else
                .forEach(System.out::println);
    }

    @Test
    public void singleRetrievingStream(){
        Optional<Integer> optNumber = List.of(1,2,3,4,5,6,7,8).stream()
//                .findAny();//akan mengambil data secara acak
                .findFirst();//hanya akan menampilkan data pertama

        optNumber.ifPresent(System.out::println);
    }

    /**
     * Ordering stream bertujuan untuk mengurutkan data
     */
    @Test
    public void orderingStream(){
        Comparator<Integer> reverseComparator = Comparator.reverseOrder();
        List.of(1,2,3,4,5,6,7,8).stream()
//                .sorted()//menggunakan feature bawaan untuk mengurutkan dari kecil ke besar
                .sorted(reverseComparator)//membuat comparator sendiri untuk kebalikan dari besar ke kecil
                .forEach(System.out::println);
    }

    /**
     * Aggregate operation
     * berguna untuk mencari nilai min max dan jumlah data
     */
    @Test
    public void aggregateStream(){
        List.of(1,2,3,4,5,6,7,8).stream()
//                .min(Comparator.naturalOrder())//mencari nilai terkecil
                .max(Comparator.naturalOrder())//mencari nilai terbesar
                .ifPresent(System.out::println);

        System.out.println("Counting");
        Long count = List.of(1,2,3,4,5,6,7,8).stream()
                .count();
        System.out.println("count number = "+count);

        System.out.println("Reduce Number");
        var reduce = List.of(1,2,3,4,5,6,7,8).stream()
                .reduce(1,(value, item) -> value * item);
//                .reduce(0,(value, item) -> value + item);
        System.out.println("Reduce ="+reduce);
    }

    /**
     * match bertujuan untuk mencari nilai yang ada
     * apakah sesuai dengan condisi yang dibuat
     * untuk response berupa boolean
     */
    @Test
    public void matchOperation(){
        boolean match = List.of(1,2,3,4,5,6,7,8).stream()
                .noneMatch(number -> number > 100); //true karena 100 tidak ada didalam list number
//                .allMatch(number -> number >= 1); // true karena all data lebih dari sama dengan 1
//                .anyMatch(number -> number > 6); //true karena ada value yang lebih dari 6

        System.out.println("match = "+ match);
    }

    /**
     * foreach opeartion
     * untuk melakukan loopin tapi di stream di bagi menjadi 2
     * foreach = merupakan terminal operator sehingga bisa langsung tampil
     * peek = mengembalikan void sehingga tidak bisa langsung tampil
     */
    @Test
    public void peekOperation(){
        List.of(1,2,3,4,5,6,7,8).stream()
                .peek(number -> System.out.println("List number : "+number)) //mengembalikan stream
                .forEach(System.out::println);
    }

    /**
     * Primitive data untuk stream yang premetive
     * int, long dan double
     * untuk primitive lebih sederhana untuk mengurutkan data karena bisa tampa menggunakan aggregate
     */
    @Test
    public void primitive(){
        IntStream intNumber = IntStream.range(1,5);
        intNumber.forEach(System.out::println);
        LongStream longNumber = LongStream.of(1,2,3,4,5);
        longNumber.forEach(System.out::println);
        DoubleStream doubleNumber = DoubleStream.builder().add(0.1).add(0.2).add(0.3).build();
        doubleNumber.forEach(System.out::println);

        System.out.println("average value number :");
        IntStream intNumbers = IntStream.range(1,50);
        OptionalDouble optDoubleNumber =  intNumbers.average(); //mereturn dengan nilai double
        optDoubleNumber.ifPresent(System.out::println);

        System.out.println("Change Number type :");
        IntStream.range(1,50)
                .mapToObj(number -> "Number :"+ String.valueOf(number))
                .forEach(System.out::println);
    }

    @Test
    public void streamToCollection(){
        //merupakan stream
        Stream<String> employeeStream = Stream.of("Angga","Anggi","Andang");

//        Set<String> employeeSet = employeeStream.collect(Collectors.toSet());
//        System.out.println("Employee Set ="+ employeeSet);
//
//        List<String> employeeList = employeeStream.collect(Collectors.toList());
//        System.out.println("Employee List ="+employeeList);
//        List<String> employeeListImutable = employeeStream.collect(Collectors.toUnmodifiableList());
//        System.out.println("Employee List Imutable ="+ employeeListImutable);


        Map<String, Integer> employeeMap = employeeStream.collect(Collectors.toMap(
                name -> name,//yang dijadikan key
                name -> name.length() //yang dijadikan value
        ));
        System.out.println("Employee Map ="+ employeeMap);


        //grouping
        Map<String, List<Integer>> numberGroup = Stream.of(1,2,3,4,5,6,7,8)
                .collect(Collectors.groupingBy(number -> number > 5 ? "Besar":"Kecil"));
//                .collect(Collectors.groupingBy(number -> {
//                 if (number>5)return "Besar";
//                 else return "Kecil";
//                }));
        System.out.println("Result Number Group ="+numberGroup);

        System.out.println("Number Partition");
        Map<Boolean, List<Integer>> numberPartition = Stream.of(1,2,3,4,5,6,7,8)
                .collect(Collectors.partitioningBy(number -> number > 5));
        System.out.println("Result Number Group ="+numberPartition);
    }

    @Test
    public void concurrentStream(){
        Stream.of(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20)
                .parallel()//dengan menggunakan ini maka akan berjalan secara concurrent
                .forEach(number -> {
                    System.out.println(Thread.currentThread().getName()+ ":"+number);
                });
    }

}


