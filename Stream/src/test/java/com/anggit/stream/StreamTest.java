package com.anggit.stream;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * Stream adalah aliran data
 * perlu di ingat bahwa sream hanya sekali mengalir seperti air
 */
public class StreamTest {
    @Test
    public void simpleStream(){
        System.out.println("Simple Stream");
        //membuat datanya kosong
        Stream<String> emptyData = Stream.empty();
        //wajib 1 data dan tidak boleh kosong jika menggunakan stream of
        Stream<String> oneData = Stream.of("Anggit");
        //data bisa null atau ada
        Stream<String> nullData = Stream.ofNullable(null);

        //Array
        System.out.println("Array Stream");
        Stream<String> arrayString = Stream.of("Anggit", "Ani", "Ana", "Andi");
        String[] employee = new String[]{"Anggit", "Ani", "Ana", "Andi"};
        Stream<String> arrayEmployee = Arrays.stream(employee);

        //foreach
        System.out.println("Foreach Stream");
//        arrayEmployee.forEach(data ->{
//            System.out.println(data);
//        });
        // with method reference
        arrayEmployee.forEach(System.out::println);


        //collection list to stream
        System.out.println("Collection Stream");
        Collection<String> employeeCollection = List.of("Anggit", "Ani", "Ana", "Andi");
        Stream<String>  employeeString = employeeCollection.stream();
        employeeString.forEach(s -> {
            System.out.println(s);
        });
    }

    /**
     * infinite berguna untuk devine value yang terus ada
     */
    @Test
    public void infiniteStream(){
        //akan menampilkan budi terus menerus jika di panggil
        Stream<String> name = Stream.generate(() -> "Budi");
//        name.forEach(System.out::println);

        //akan selalu loping menambahkan nilai aval seperti counting hingga tidak terhingga
        Stream<Integer> numberList = Stream.iterate(1, value -> value + 1);
//        numberList.forEach(System.out::println);
    }

    @Test
    public void streamBuilder(){
        Stream.Builder<String> builder = Stream.builder();
        //untuk menambahkan 1 value
        builder.accept("Seto");
        //untuk menambahkan beberapa value
        builder.add("Siti").add("Sintya").add("Suhardi");

        Stream<String> employee = builder.build();
        employee.forEach(System.out::println);
    }

    /**
     * stream operation tujuan untuk merubah nilai yang ada di dalam stream
     * demgan membentuk penyimpanan baru sehingga data aslinya tidak berubah
     */
    @Test
    public void streamOperation(){
        Stream.Builder<Object> employeeBuilder = Stream.builder().add("Andi").add("Agung");
        Stream<Object> employee= employeeBuilder.build();
        Stream<String> employeeString = employee.map(name -> name.toString());
        Stream<String> employeeUpperCase = employeeString.map(name -> name.toUpperCase());
        employeeUpperCase.forEach(System.out::println);
    }
}
