package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.*;

public class MetaDataTest {

    @Test
    public void databaseMetaData() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        DatabaseMetaData databaseMetaData = connection.getMetaData(); //ini yang berguna untuk mendapatkan informasi dari database

        System.out.println(databaseMetaData.getDatabaseProductName());
        System.out.println(databaseMetaData.getDatabaseProductVersion());//menampilkan database version yang dipakai

        ResultSet resultSet = databaseMetaData.getTables("belajarJDBC", null,null,null); //untuk memilih db yang di inginkan
        while (resultSet.next()){
            String tableName = resultSet.getString("TABLE_NAME"); //untuk mengambil table name dari database
            System.out.println("Table :"+tableName);
        }
        connection.close();
    }

    @Test
    public void resultSetMetaData() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from comments");

        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        for (int i = 1; i<= resultSetMetaData.getColumnCount(); i++){
            System.out.println("Name :"+ resultSetMetaData.getColumnName(i));
            System.out.println("Type :"+ resultSetMetaData.getColumnType(i)); //size dari type data yang digunakan
            System.out.println("Type Name :"+ resultSetMetaData.getColumnTypeName(i)); //type data
        }
        resultSet.close();
        statement.close();
        connection.close();
    }
}
