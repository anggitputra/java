package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.*;

public class PrepareStatmentTest {

    /**
     * prepare statement untuk menangani sql injection
     * @throws SQLException
     */
    @Test
    public void prepareStatementTest() throws SQLException {
        String username = "admin'; #";
        String password = "admin";

        Connection connection = ConnectionUtils.getDataSource().getConnection();
        String sql = "select * from admin where username = ? and password = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql); //untuk prepare statement
        preparedStatement.setString(1,username); //untuk insert datanya sebagai checking juga handle sql injection
        preparedStatement.setString(2,password); //set statement ini harus di sesuaikan dengan juma yang ?

        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) System.out.println("Success Login ");
        else System.out.println("Gagal login");

        preparedStatement.close();
        connection.close();
    }

    @Test
    public void autoIncrementTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        String sql = "insert into comments(email, comment) values (?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); //di set generate key terlebih dahulu
        preparedStatement.setString(1,"Andang@gmail.com");
        preparedStatement.setString(2,"Test Increment");
        preparedStatement.executeUpdate();

        ResultSet resultSet = preparedStatement.getGeneratedKeys(); //untuk mengambil generatet key
        if (resultSet.next()) System.out.println("Id Comment : "+ resultSet.getInt(1));

        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
