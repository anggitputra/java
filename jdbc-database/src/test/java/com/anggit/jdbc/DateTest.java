package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.*;

public class DateTest {
    @Test
    public void dateTimeTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        String sql = "insert into sample_time(sample_date,sample_time,sample_timestamp) values (?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setDate(1,new Date(System.currentTimeMillis()));
        preparedStatement.setTime(2,new Time(System.currentTimeMillis()));
        preparedStatement.setTimestamp(3, new Timestamp(System.currentTimeMillis()));

        preparedStatement.executeUpdate();

        preparedStatement.close();
        connection.close();
    }

    @Test
    public void queryDateTimeTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        String sql = "select * from sample_time";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()){
            Date date = resultSet.getDate("sample_date");
            System.out.println("Date = "+ date);
            Time time = resultSet.getTime("sample_time");
            System.out.println("Time = "+ time);
            Timestamp timestamp = resultSet.getTimestamp("sample_timestamp");
            System.out.println("Timestamp = "+ timestamp);
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}