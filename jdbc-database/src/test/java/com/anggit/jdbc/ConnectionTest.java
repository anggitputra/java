package com.anggit.jdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionTest {
    @BeforeAll
    static void registerTest(){
        try {
            Driver mysqlDriver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(mysqlDriver);
        } catch (Exception e){
            Assertions.fail(e);
        }
    }

    /**
     * connection manual ini tidak direkomendasikan karena memakan banyak resource
     */
    @Test
    public void connection(){
        String jdbcURL = "jdbc:mysql://localhost:3306/belajarJDBC?serverTimezone=Asia/Jakarta";
        String username = "root";
        String password = "root";
        try {
            Connection connection = DriverManager.getConnection(jdbcURL,username,password);
            System.out.println("Success Connect To Database");
            connection.close();//berguna untuk menutup connection karena jika aplikasi running kalo tidak di close bisa menghabiskan limit
            //limit mysql 151
            System.out.println("Success Connect To Database");
        }catch (SQLException exception){
            Assertions.fail(exception);
        }
    }
}
