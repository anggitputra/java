package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * peringatan untuk penggunaan batch
 * maksimal sekali kirim ke db 1000
 * jadi jika ada data diatas itu lebih baik di bagi
 */
public class BatchTest {

    @Test
    public void batchTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        Statement statement = connection.createStatement();

        String sql = "insert into comments(email, comment) values ('Angga@gmail.com', 'Test Batch')";
        for (int i=0; i<1000; i++){
            statement.addBatch(sql); //untuk insert ke batch sehingga batch akan menampung data yang di masukkan
        }
        //kecepatan prosessing tergantung laptop, pc atau server yang dipakai
        statement.executeBatch(); //untuk mengirim data yang di batch ke database secara bersamaan
        statement.close();
        connection.close();
    }

    @Test
    public void prepareStatementBatchTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        String sql = "insert into comments(email, comment) values (?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        for (int i=0; i<1000; i++){
            preparedStatement.clearParameters(); //untuk menghapus parameter sebelumnya dan akan di isi baru
            preparedStatement.setString(1,"Budi@gmail.com");
            preparedStatement.setString(2,"testing prepare statement");
            preparedStatement.addBatch();
        }
        //kecepatan prosessing tergantung laptop, pc atau server yang dipakai
        preparedStatement.executeBatch(); //untuk mengirim data yang di batch ke database secara bersamaan
        preparedStatement.close();
        connection.close();
    }
}
