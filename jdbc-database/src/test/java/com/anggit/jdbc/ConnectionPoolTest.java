package com.anggit.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolTest {

    /**
     * connection poll sangat disarankan untuk pembuatan project yang connect ke database
     * karena ini juga bisa meringankan beban dari database server
     */
    @Test
    public void hikariCPTest(){
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.cj.jdbc.Driver"); //sesuai dengan driver yang digunakan
        config.setJdbcUrl("jdbc:mysql://localhost:3306/belajarJDBC?serverTimezone=Asia/Jakarta");
        config.setUsername("root");
        config.setPassword("root");

        config.setMaximumPoolSize(10); //setting maksimal pool yang connect ke database
        config.setMinimumIdle(5); //pool yang dibiarkan tetap aktif
        config.setIdleTimeout(60_000); //jika pool tidak digunakan selama 1 detik maka akan di close
        config.setMaxLifetime(10 * 60_000); //setting membatasi umur dari pool
        //masih banyak lagi dan featurenya bisa diihat di git

        try {
            HikariDataSource dataSource = new HikariDataSource(config);
            Connection connection = dataSource.getConnection();
            connection.close(); //ini untuk menutup connection ke db dan mengembalikan ke poll
            dataSource.close(); //ini untuk menutup semua connection yang ada di poll
        }catch (SQLException exception){
            Assertions.fail(exception);
        }
    }

    @Test
    public void utilsTest() throws SQLException{
        Connection connection = ConnectionUtils.getDataSource().getConnection();
    }


}
