package com.anggit.jdbc;

import com.anggit.jdbc.entity.Comment;
import com.anggit.jdbc.repository.CommentRepository;
import com.anggit.jdbc.repository.CommentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * Repository = pattern yang paling sering digunakan saat ini
 * Dao = Data akses object ini banyak digunakan sudah lama dan saat ini udah tergantian repository
 *
 * storage procedure = sangat jarang digunakan dan lebih baik jangan di implementasikan
 */
public class RepositoryTest {

    private CommentRepository commentRepository;

    @BeforeEach
    public void setUp(){//agar di jalankan pertama kali
        commentRepository = new CommentRepositoryImpl();
    }

    @Test
    public void insertTest(){
        Comment comment = new Comment("Gilang@gmail.com","Hallo Dunia");
        commentRepository.insert(comment);

        Assertions.assertNotNull(comment.getId());
        System.out.println("Insert Success ="+comment.getId());
    }

    @Test
    public void findByIdTest(){
        Comment comment = commentRepository.findById(2001);
        Assertions.assertNotNull(comment);
        System.out.println("ID = "+comment.getId()+" Email ="+ comment.getEmail()+ " Comment ="+ comment.getComment());
    }

    @Test
    public void findAll(){
        List<Comment> comments = commentRepository.findAll();
        System.out.println("All Data ="+comments.size());
    }

}
