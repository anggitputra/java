package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementTest {

    @Test
    public void testCreateStatement() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();

        Statement statement = connection.createStatement(); //ini untuk membuat statement edit, delete dll

        statement.close(); //jika tidak ada yang digulakukan lagi harus di close

        connection.close(); //tutup dan kembalikan ke pool
    }


    @Test
    public void testExecuteUpdate() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();

        Statement statement = connection.createStatement();

        String update = "insert into customers(id, name, email) " +
                "values ('ev', 'evi', 'evi@yahoo.com')"; //insert 1 per satu

        int resUpdate = statement.executeUpdate(update);
        System.out.println("result update ="+resUpdate);

        statement.close();

        connection.close();
    }

    @Test
    public void testExecuteDelete() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();

        Statement statement = connection.createStatement();

        String delete = "delete from customers"; //delete all data customers

        int resDelete = statement.executeUpdate(delete); //berguna untuk insert, delete, update
        System.out.println("result update ="+resDelete);

        statement.close();
        connection.close();
    }

    @Test
    public void testExecuteQuery() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();

        Statement statement = connection.createStatement();

        String sql = "select * from customers";
        /**
         * result set sebagai penampung data hasil query
         * ResultSet bisa maju(next) dan mundur(previous)
         * ketika ada join ke table lain supaya tidak ambigu di kasih alias
         */
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()){
            String id, name, email;
            id = resultSet.getString("id");
            name = resultSet.getString("name");
            email = resultSet.getString("email");// get berdasarkan tipe data

            System.out.println(String.join(",",id,name,email));
        }

        resultSet.close();
        statement.close();
        connection.close();
    }
}
