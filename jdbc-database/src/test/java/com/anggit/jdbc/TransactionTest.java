package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionTest {
    @Test
    public void prepareStatementBatchTest() throws SQLException {
        Connection connection = ConnectionUtils.getDataSource().getConnection();
        connection.setAutoCommit(false); //default disini adalah true

        /**
         * ketika ini dijalankan hanya akan disimpan dicache
         */
        String sql = "insert into comments(email, comment) values (?, ?)";
        for (int i=0; i<10; i++){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,"Budiman@gmail.com");
            preparedStatement.setString(2,"testing transactional");
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }

        connection.commit();//ini yang berguna untuk menyimpan ke db tanpa ini tidak akan tersimpan
        connection.rollback();//untuk menghapus ulang data yang telah disimpan
        connection.close();
    }
}
