package com.anggit.jdbc;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlInjection {
    @Test
    public void testExecuteQuery() throws SQLException {
//        String username ="admin";
//        String password ="admin";

        /**
         * contoh sql injection
         */
        String username ="admin'; #";
        String password ="adminhh";

        Connection connection = ConnectionUtils.getDataSource().getConnection();
        Statement statement = connection.createStatement(); //statement sangat rawan di serang sql injection jadi hanya cocok ketika data berasal dari dalam
        String sql = " select * from admin where" +
                " username = '"+username+"' and password = '"+ password+"'";
        System.out.println("Sql String ="+ sql);
        //select * from admin where username = 'admin'; #' and password = 'adminhh'
        // # ke belakang tidak dipakai dianggap sebagai commentar

        ResultSet resultSet = statement.executeQuery(sql);
        if (resultSet.next()) System.out.println("Sukses login :"+ resultSet.getString("username"));
        else System.out.println("Gagal Login");
        resultSet.close();
        statement.close();
        connection.close();
    }

}
