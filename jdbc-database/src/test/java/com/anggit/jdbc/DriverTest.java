package com.anggit.jdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Driver;
import java.sql.DriverManager;

public class DriverTest {

    @Test
    public void registerTest(){
        try {
            //driver mysql bisa juga mendaftarkan yang lainya
            Driver mysqlDriver = new com.mysql.cj.jdbc.Driver(); //versi yang terbaru
            //registrasi driver ini sebagai contoh mysql
            DriverManager.registerDriver(mysqlDriver);
        } catch (Exception e){
            Assertions.fail(e);
        }
    }
}
