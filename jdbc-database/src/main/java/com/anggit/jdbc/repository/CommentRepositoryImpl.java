package com.anggit.jdbc.repository;

import com.anggit.jdbc.ConnectionUtils;
import com.anggit.jdbc.entity.Comment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository{
    @Override
    public void insert(Comment comment) {
        try(Connection connection = ConnectionUtils.getDataSource().getConnection()) { //connect to database
            String sql = "insert into comments(email, comment) values (?,?)";
            try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
                statement.setString(1, comment.getEmail());
                statement.setString(2, comment.getComment());
                statement.executeUpdate();

                try (ResultSet resultSet = statement.getGeneratedKeys()){
                    if (resultSet.next()){
                        comment.setId(resultSet.getInt(1)); //ini untuk mengembalikan id setelah berhasil insert
                    }
                }
            }
        }catch (SQLException exception){
            throw new RuntimeException(exception);
        }
    }

    @Override
    public Comment findById(Integer id) {
        try(Connection connection = ConnectionUtils.getDataSource().getConnection()) {
            String sql ="select * from comments where id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)){
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()){
                    if (resultSet.next()){
                        return new Comment(
                          resultSet.getInt("id"),
                          resultSet.getString("email"),
                          resultSet.getString("comment")
                        );
                    }else {
                        return null;
                    }
                }
            }
        }catch (SQLException exception){
            throw new RuntimeException(exception);
        }
    }

    @Override
    public List<Comment> findAll() {
        try (Connection connection = ConnectionUtils.getDataSource().getConnection()){
            String sql = "select * from comments";
            try (Statement statement = connection.createStatement()){
                try (ResultSet resultSet = statement.executeQuery(sql)){
                    List<Comment> comments = new ArrayList<>();
                    while (resultSet.next()){
                        comments.add(new Comment(
                                resultSet.getInt("id"),
                                resultSet.getString("email"),
                                resultSet.getString("comment")
                        ));
                    }
                    return comments;
                }
            }
        }catch (SQLException exception){
            throw new RuntimeException(exception);
        }
    }
}
