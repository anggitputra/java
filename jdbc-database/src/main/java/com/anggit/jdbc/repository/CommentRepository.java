package com.anggit.jdbc.repository;

import com.anggit.jdbc.entity.Comment;

import java.util.List;

/**
 * repository interface sebagai penyedia method yang akan di
 * consume commentRepository Impl
 */
public interface CommentRepository {
    void insert(Comment comment);

    Comment findById(Integer id);

    List<Comment> findAll();
}
