package com.anggit.maven;

import com.google.gson.Gson;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Gson gson = new Gson(); //external dependency

        Person person = new Person("Anggit Putra");

        String json = gson.toJson(person); //convert to json string

        System.out.println(json);
    }

}
